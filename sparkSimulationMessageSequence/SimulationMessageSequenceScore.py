from common.DataAccessLayer.DataAccessLayer import MessageAccessor
from common.DataAccessLayer.DataAccessLayer import DSEAccessor
from common.DataAccessLayer.DataAccessLayer import LearningAccessor

import pandas as pd
import h2o
import os
import rpy2.robjects as robjects
from rpy2.robjects.conversion import localconverter
from rpy2.robjects import pandas2ri
from sparkSimulationMessageSequence.LearningPropertiesKey import  LearningPropertiesKey
from common.DataAccessLayer.SparkDataAccessLayer import SparkDSEAccessor
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
import time
import numpy as np
import logging
import boto3
import threading

from pysparkling import *

# Constants
MSO_EXCEL_FILE_PREFIX = "messageSequenceDriver.r"
TARGET_TYPE = "LE_MS_messageAnalysisTargetType"
SEQ_AVG_PROBABILITY_COL = "avg_seq_prob"
ACCOUNT_ID_COL = "accountId"
MAX_MESSAGE_SEQ_LENGTH = 10
ALREADY_SENT_MSG_PREPEND_COUNT = 3


OPEN_PREFIX = "OPEN..."
SEND_PREFIX = "SEND..."

# Constants required for H2O
H2O_IP = "127.0.0.1"
H2O_PORT = 54321

#H2O_IP = "10.0.0.209"
#H2O_PORT = 54321
DESIGN_MATRIX_R_SCRIPT_PATH = 'sparkSimulationMessageSequence/DesignMatrixBuilder.R'
MODEL_S3_FILE_NAME = "model_s3_path_map.csv"
S3_BUCKET_NAME = "learning-mso-models"

logger = None
spark = None
hc =  None

class MessageSequencePredictor:
    """
    This class is responsible for scoring and predicting the scores for the sequences generated in the database
    """

    simulation_run_uid = None
    learning_home_dir = None
    mso_build_uid = None
    build_dir = None
    learning_properties = None
    model_file_map = None
    design_matrix = None
    model_s3_path_map = None
    design_matrix_spark = None

    # This dictionary holds the results generated for message sequences
    result_cache = {}

    def __init__(self, simulation_run_uid, learning_properties, learning_home_dir, mso_build_uid, simulation_logger_name, spark1):
        """
        Initialization for the Message Sequence Predictor class
        """

        try:

            global logger
            global spark
            global hc

            logger = logging.getLogger(simulation_logger_name)
            spark = spark1
            hc = H2OContext.getOrCreate(spark)

            self.simulation_run_uid = simulation_run_uid
            self.learning_home_dir = learning_home_dir
            self.mso_build_uid = mso_build_uid
            self.learning_properties = learning_properties

            #print(learning_home_dir, mso_build_uid)
            self.build_dir = os.path.join(learning_home_dir, "builds", mso_build_uid)

            start_time = time.time()
            self.__read_message_model_map()
            logging.debug("Time taken to read Model Map: " + str(time.time() - start_time))

            # Initialize H2O server
            # In case of sparkling water initialization is not needed
            # self.__initialize_h2o_server()

            start_time = time.time()
            self.__read_design_matrix()
            logging.debug("Time taken to read design Matrix: " + str(time.time() - start_time))

        except:

            logger.error("Error while initialization for MessageSequencePredictor")
            # Release the H2O server in case there is error in initialization
            self.release_h2o_server()
            raise

    def __write_s3_file_map(self):
        """

        :return:
        """
        if self.model_s3_path_map.empty:
            file_path = os.path.join(self.build_dir, MODEL_S3_FILE_NAME)
            self.model_s3_path_map.to_csv(file_path, index=False)

    def __read_message_model_map(self):
        """
        This function read a CSV file and maintains map of target name to model file path on the disk
        :return:
        """

        # Initialize s3 map data frame
        self.model_s3_path_map = pd.DataFrame(columns=['messageUID', 'S3Path'])

        # Initialize model data frame
        self.model_file_map = pd.DataFrame(columns=['target', 'modelName'])

        # Read the Model to S3 Path file if exists
        model_s3_path_map_file = os.path.join(self.build_dir, MODEL_S3_FILE_NAME)
        if os.path.isfile(model_s3_path_map_file):
            self.model_s3_path_map = pd.read_csv(model_s3_path_map_file)
        else:
            # Excel file containing mapping
            excel_file_name = MSO_EXCEL_FILE_PREFIX + "_" + self.learning_properties.get_property('buildUID') + ".xlsx"
            excel_file_path = os.path.join(self.build_dir, excel_file_name)
            self.model_file_map = pd.read_excel(excel_file_path, sheet_name=2)

            # Drop unnecessary columns
            required_cols = ['target', 'modelName']
            self.model_file_map = self.model_file_map[required_cols]

    def __initialize_h2o_server(self):
        """
        This function will get the available H2O server and initialize it
        :return:
        """
        global H2O_IP
        global H2O_PORT

        learning_accessor = LearningAccessor()
        H2O_IP, H2O_PORT = learning_accessor.get_available_h2o_server()

        if not H2O_IP:
            logger.info("No H2O server available. Using local H2O server for simulation.")
            H2O_IP = "127.0.0.1"
            H2O_PORT = 54321
            # exit()

        logger.info("Initializing H2O server with IP=" + H2O_IP + "and PORT=" + str(H2O_PORT))

        # Initialize and connect to H2O
        h2o.init(ip=H2O_IP, port=H2O_PORT)
        h2o.connect(ip=H2O_IP, port=H2O_PORT)

        logger.info("Initialized H2O server with IP=" + H2O_IP + "and PORT=" + str(H2O_PORT))

    def release_h2o_server(self):
        """
        This function will release the H2O server if any is in current use.
        :return:
        """
        """
        NOTE: This is not required in case of sparkling water H2O
        logger.info("Releasing H2O server..")

        global H2O_IP
        global H2O_PORT

        if H2O_IP and H2O_PORT:
            
            learning_accessor = LearningAccessor()
            learning_accessor.release_h2o_server(H2O_IP, H2O_PORT)
            logger.info("H2O server relaease IP=" + H2O_IP + " and PORT=" + str(H2O_PORT))
            H2O_IP = None
            H2O_PORT = None

        else:
            logger.info("There is no H2O server to release")
        """


    def __insert_traversed_messsage_cols_v2(self, h2o_design_matrix_df, simulation_result, message_uid_list, message_position):
        """
        This function updates H2O data frame with already traversed messages (i.e. messages already added in the sequence)
        :param h2o_design_matrix_df:
        :param simulation_result:
        :return: updated H2O data frame
        """

        for message_index in range(1, message_position):

            logger.info("Traversing columns for message Position: " + str(message_index))

            for message_uid in message_uid_list:

                open_target = OPEN_PREFIX + message_uid
                send_target = SEND_PREFIX + message_uid
                # account_uids = simulation_result[simulation_result[str(message_index)] == message_uid][ACCOUNT_ID_COL].tolist()
                # #print(account_uids)
                # #print(design_matrix)
                # indices = design_matrix[ACCOUNT_ID_COL].isin(account_uids)
                # print(indices)
                h2o_design_matrix_df[simulation_result.index[simulation_result[str(message_index)] == message_uid].tolist(), open_target] = 1
                h2o_design_matrix_df[simulation_result.index[simulation_result[str(message_index)] == message_uid].tolist(), send_target] = 1

        return h2o_design_matrix_df

    def predict_for_sequences_v2(self):
        """
        This method generates the probability based on greedy approach for message sequences
        """
        try:
            product_uid = self.learning_properties.get_property(LearningPropertiesKey.PRODUCT_UID)

            # Get the Non-expired messages to simulate for product
            logger.info("Fetching messages for the product ... ")
            message_accessor = MessageAccessor()
            complete_message_uid_list = message_accessor.get_non_expired_message_list(product_uid)
            logger.info("Fetched messages for the product")

            logger.info("Processing " + str(len(complete_message_uid_list)) + "Messages for simulation..")
            self.process_message_uid_list(complete_message_uid_list)
            logger.info("Simulation for accounts completed with LearningRunUID- " + str(self.simulation_run_uid))

        except:

            # Release the H2O server in use
            self.release_h2o_server()
            raise

    def process_message_uid_list(self, message_uid_list):
        """
        This function simulates the list of message uids specified.
        :param  message_uid_list: list of message uids
        :return: None
        """

        # Get productUID to for current simulation
        product_uid = self.learning_properties.get_property(LearningPropertiesKey.PRODUCT_UID)

        # Sequence length minimum value among max length and message list size
        sequence_length = min(MAX_MESSAGE_SEQ_LENGTH, len(message_uid_list))

        simulation_results = pd.DataFrame()
        simulation_results[ACCOUNT_ID_COL] = list(self.design_matrix.select(ACCOUNT_ID_COL).toPandas()[ACCOUNT_ID_COL])
        simulation_results['probability'] = 0
        simulation_results['sequence'] = ""
        simulation_results['sequence_length'] = 0

        # Check if data loading thread has completed before proceeding to fetch the data
        for thread in threading.enumerate():
            if thread.name == "SimulationDataLoader" and thread.is_alive():
                logger.info("Waiting for Data Loading process to complete..")
                thread.join()

        # Fetch the account-email interaction table data
        start_time = time.time()
        logger.info("Reading account message interactions.. ")
        learning_accessor = LearningAccessor()
        account_message_interaction_df = learning_accessor.get_account_message_interactions(product_uid)
        logger.info("Completed reading interactions in time = " + str(time.time() - start_time))

        for message_position in range(1, sequence_length+1):

            sequence_prediction_df = pd.DataFrame()

            # # Create deep copy for design matrix (pandas dataframe)
            # start_time = time.time()
            # input_df = self.design_matrix.copy(deep=True)
            # logger.debug("1. Time taken to deep copy Pandas DataFrame- " + str(time.time() - start_time))



            # Convert Spark dataframe to H2O frame
            start_time = time.time()
            input_h2o_df = hc.as_h2o_frame(self.design_matrix, 'input_h2o_df')
            logger.debug("3. Time taken to convert H2O Frame- " + str(time.time() - start_time))

            # Update design Matrix based on previous messages in the order
            start_time = time.time()
            updated_input_h2o_df = self.__insert_traversed_messsage_cols_v2(input_h2o_df, simulation_results,
                                                                message_uid_list, message_position)
            logger.debug("2. Time taken to add traversed cols- " + str(time.time() - start_time))

            for message_uid in message_uid_list:

                # Get model for the message
                model = self.__get_model_for_message(message_uid)
                target_name = message_uid
                logger.debug("4. Predicting for message" + target_name)

                if model:
                    """
                    If model file is present and model is loaded use that for predictions
                    """
                    # Get columns used for training
                    columns = self.__get_model_variable_list(model)

                    # Create a deep copy for testing data frame
                    predict_model_h2o_df = h2o.deep_copy(updated_input_h2o_df, 'predict_model_h2o_df')

                    # Convert input data matrix to comply with model columns
                    start_time = time.time()
                    predict_model_h2o_df = self.__convert_design_matrix(predict_model_h2o_df, columns)
                    logger.debug("4.1 Time taken to Compatible H2O Frame- " + str(time.time() - start_time))

                    # Drop Nan Values
                    predict_model_h2o_df = predict_model_h2o_df.na_omit()

                    # Convert the dataframe to numeric
                    predict_model_h2o_df = predict_model_h2o_df.asnumeric()

                    # Predict the probability for the
                    start_time = time.time()
                    prediction = model.predict(predict_model_h2o_df)
                    logger.debug("4.2 Time taken to Predict from Model- " + str(time.time() - start_time))

                    # Convert the predicted probability into pandas dataframe
                    sequence_prediction_df[target_name] = prediction[:, 'p1'].as_data_frame()['p1']

                else:
                    logger.info("4.1 Model file not found for message " + target_name)
                    # Add scores with zeros to the results
                    sequence_prediction_df.loc[:, target_name] = np.zeros(len(simulation_results))
                    # Instead of zero put minimum positive value
                    #sequence_prediction_df.loc[:, target_name] = np.nextafter(0, 1)

                # Update sequence prediction based on traversed messages
                start_time = time.time()
                sequence_prediction_df = self.__update_results_for_travesed_messages(sequence_prediction_df,
                                                                                     simulation_results, message_uid,
                                                                                     message_uid_list,message_position)
                logger.debug("5. Time taken to update predict results for traversed message- " + str(time.time() - start_time))

                # Update sequence prediction for already sent messages
                start_time = time.time()
                sequence_prediction_df = self.__update_results_for_already_sent_messages(sequence_prediction_df, simulation_results,
                                                                                         account_message_interaction_df, target_name)
                logger.debug("6. Time taken to update predict results for sent message- " + str(time.time() - start_time))

            """
            NOTE: Simulation generate probability for all account and all messages for every position. This segment is 
            executed after scoring all messages for position `i`. It stored in `sequence_prediction_df` data frame.
            
            At ith position score will be `NaN` if that message is sent earlier or used in sequence at any position 
            i to i-1. In the result i.e. `simulation_result` data frame we put `None` as messageUID at that position
            for message with score NaN.  
            """

            # Get the maximum probability messages uid at that position for each account
            max_index = sequence_prediction_df.idxmax(axis=1, skipna=True).fillna('None')
            simulation_results[str(message_position)] = max_index

            # Get the maximum probability value at the position for each account
            simulation_results[str(message_position)+"_score"] = sequence_prediction_df.max(axis=1, skipna=True).fillna(0)
            simulation_results['probability'] = simulation_results['probability'] + simulation_results[str(message_position)+"_score"]

            # Find non-None message accounts
            non_none_indices = simulation_results[str(message_position)] != 'None'

            """
            NOTE: In order to avoid going through messages one by one while writing to the database we create string as
            message sequence separated by `;` character. We append only message that are not `None`.
            """
            # Update the sequence with ';' separated values to avoid post processing step
            simulation_results.loc[non_none_indices, 'sequence'] = simulation_results.loc[non_none_indices, 'sequence'] + ";" + simulation_results.loc[non_none_indices, str(message_position)]
            simulation_results.loc[non_none_indices, 'sequence_length'] = simulation_results.loc[non_none_indices, 'sequence_length'] + 1

        # Average probability for the message sequence
        simulation_results['probability'] = simulation_results['probability'] / simulation_results['sequence_length']
        """
        NOTE: In order to avoid divide by zero and get inf in probability value. Update the np.inf to zero 
        """
        simulation_results.loc[simulation_results['probability'] == np.inf, 'probability'] = 0
        # TO-TEST::
        # simulation_results.to_csv("~/simulation_results_991.csv", index=False)

        # Release the H2O server before writing results to database
        self.release_h2o_server()

        # Write S3 file map to the disk
        self.__write_s3_file_map()

        # Append already sent messages to the simulation result
        simulation_results = self.__prepend_already_sent_messages_to_seq(simulation_results, account_message_interaction_df)

        self.__post_process_simulation_results(simulation_results)

    def __prepend_already_sent_messages_to_seq(self, simulation_results, account_message_interaction_df):
        """
        This function finds the 'N' latest intearaction for each account in simulation result and prepend the
        interaction message(s) to the resultant message sequence generated from simulation.

        :param simulation_results: Pandas data frame holding simulation results
        :param account_message_interaction_df: Pandas data frame holding account interactions
        :return: updated simulation result data frame where message sequences are prepended
        """

        """ 
        NOTE: `account_message_interaction_df` data frame is already filtered for specific product we are running
        simulation for.
        """
        # Get only necessary columns from account interaction data frame
        interaction_df = account_message_interaction_df[['messageUID', 'accountUID', 'emailSentDate']]

        # Keep only columns with account id in the simulation result data frame
        interaction_df = interaction_df[interaction_df.accountUID.isin(simulation_results[ACCOUNT_ID_COL])].reset_index(drop=True)

        # Sort the data frame to put latest interactions on top
        """
        NOTE: The sorting operation needs to be done before removing duplicate account-message pairs. As remove duplicates may
        remove latest pair of the interaction. Later, when top latest interactions for the account is select this 
        message could be omitted because latest interaction pair is already been dropped. 
        """
        interaction_df = interaction_df.sort_values(by='emailSentDate', ascending=False).reset_index(drop=True)

        # Drop the duplicate messages for same account keep the one which are latest
        interaction_df = interaction_df.drop_duplicates(subset=['accountUID', 'messageUID'], keep='first').reset_index(drop=True)

        # Get only top `N` interactions i.e. sent emails for each account
        interaction_df = interaction_df.groupby('accountUID').head(ALREADY_SENT_MSG_PREPEND_COUNT).reset_index(drop=True)

        # Remove NaN, None or empty messageUID columns
        invalid_values = [np.NaN, None, ""]
        interaction_df = interaction_df[~interaction_df['messageUID'].isin(invalid_values)]

        # TO-TEST::
        # interaction_df.to_csv("~/interaction_df.csv")

        # Create sequence string to be pre-pended to the earlier message sequence
        # Sorting in ascending order to make sure messages are in string in order of their sent date
        interaction_df = interaction_df.sort_values(by='emailSentDate', ascending=True).groupby('accountUID')['messageUID'].apply(';'.join).reset_index()

        # TO-TEST::
        # interaction_df.to_csv("~/interaction_df1.csv")

        simulation_results = pd.merge(left=simulation_results, right=interaction_df,
                                      how='left', left_on=ACCOUNT_ID_COL, right_on='accountUID')

        # There could be accounts with no interactions hence filter them out before concatenating them
        # 'messageUID' is the added column from interaction_df after merge operation. Concatenate with sequence column
        # in simulation_result data frame.
        non_na_indices = simulation_results['messageUID'].isnull() == False

        # Message sequence string has a ';' at the start in-order to avoid checking for index =1. Hence no need to add
        # + ';' while concatenating with simulation_results.
        simulation_results.loc[non_na_indices, 'sequence'] = simulation_results.loc[non_na_indices,'messageUID'] + simulation_results.loc[non_na_indices, 'sequence']
        simulation_results.loc[non_na_indices, 'sequence'] = ";" + simulation_results.loc[non_na_indices, 'sequence'].astype(str)

        return simulation_results

    def __post_process_simulation_results(self, simulation_results):
        """
        This funciton post processes the results generated from the simulation. Makes it database table compatible and
        writes to the database.
        :param simulation_results:
        :return:
        """
        unique_message_sequences = simulation_results.sequence.unique()

        logger.info("Writing message sequences to the database..")
        message_accessor = MessageAccessor()
        message_seq_str_to_uid_map = message_accessor.write_message_sequences_v2(self.simulation_run_uid,
                                                                                 self.mso_build_uid, unique_message_sequences)

        # Add the message sequence UIDs as a column
        simulation_results['messageSeqUID'] = simulation_results['sequence'].map(message_seq_str_to_uid_map)

        # Drop unnecessary columns
        col_list = [ACCOUNT_ID_COL, 'messageSeqUID', 'probability']
        simulation_results = simulation_results[col_list]

        # Rename columns to comply with the database table
        simulation_results.rename(index=str, columns={ACCOUNT_ID_COL: "accountUID"}, inplace=True)

        logger.info("Writing account results to the database..")
        message_accessor.write_account_simulation_results_v2(self.simulation_run_uid, self.mso_build_uid, simulation_results)

    def __update_results_for_travesed_messages(self, prediction_df, simulation_result, message_uid, message_uid_list, message_position):
        """
        This functions updates the results to eliminate already used messages in the sequence. It will set the result to
        zero in case the message already used for the account.
        :param prediction_df:
        :param traversed_message_uids:
        :return:
        """

        for message_index in range(1, message_position):

            for message_uid in message_uid_list:

                prediction_df.loc[simulation_result[str(message_index)] == message_uid, message_uid] = np.NaN
                prediction_df.loc[simulation_result[str(message_index)] == message_uid, message_uid] = np.NaN

        return prediction_df

    def __update_results_for_already_sent_messages(self, prediction_df, simulation_result, account_message_interaction_df, target_name):
       """
        This function update the prediction data for the accounts for whom the messages are already been sent
       :param prediction_df: prediction data frame from the simulation
       :param simulation_result: overall simulation results data frame
       :param account_message_interaction_df: account message interaction data frame
       :param target_name: target name (ie.e message) to update the predictions for
       :return: updated prediction pandas data frame
       """
       accountUIDs = account_message_interaction_df.loc[account_message_interaction_df['messageUID'] == target_name, 'accountUID']
       prediction_df.loc[simulation_result[ACCOUNT_ID_COL].isin(accountUIDs), target_name] = np.NaN
       return prediction_df


    def predict_for_sequences(self):
        """
        This function reads the message sequences to be predicted and start the prediction for the sequences.
        :return:
        """
        # Read the message Sequences from database
        message_accessor = MessageAccessor()
        message_seq_uids = message_accessor.get_unique_message_sequence_uids(self.simulation_run_uid, self.mso_build_uid)

        # Get the MSO Build UID
        mso_build_uid = self.learning_properties.get_property(LearningPropertiesKey.BUILD_UID)

        # Final result data frame
        final_result_df = pd.DataFrame()
        final_result_df[ACCOUNT_ID_COL] = self.design_matrix[ACCOUNT_ID_COL]

        # Current sequence count
        sequence_count = 0
        # Iterate through the sequences
        for message_seq_uid in message_seq_uids:

            start_time = time.time()
            seq_result_df = self.get_probability(message_seq_uid)
            logger.debug("Time taken to predict for sequence %s is - %s : " % (message_seq_uid, str(time.time() - start_time)))

            final_result_df[message_seq_uid] = seq_result_df[SEQ_AVG_PROBABILITY_COL]
            sequence_count += 1
            logger.info("Completed {CURRENT} / {TOTAL}".format(CURRENT=sequence_count, TOTAL=len(message_seq_uids)))

        logger.info("Prediction Process completed.")

        # Write back to db
        message_accessor = MessageAccessor()

        logger.info("Writing results to database...")
        message_accessor.write_account_simulation_results(self.simulation_run_uid, mso_build_uid, final_result_df)
        logger.info("Results written in database...")

    def __read_design_matrix(self):
        """
        This function will call R-Script function responsible for generating Design Matrix for all messages which is
        used as 'allModel' in the MSO Scoring functions.
        :return:
        """
        # Prepare arguments required for the R Script
        mso_build_uid = self.learning_properties.get_property(LearningPropertiesKey.BUILD_UID)
        db_config = DatabaseConfig.instance()

        r_script_path = DESIGN_MATRIX_R_SCRIPT_PATH
        rscript_design_matrix_builder = os.path.join(self.learning_home_dir, r_script_path)

        r = robjects.r
        r.source(rscript_design_matrix_builder)
        get_design_matrix_r_func = robjects.globalenv['getDesignMatrixForSimulation']
        rdf_design_matrix = get_design_matrix_r_func(self.learning_home_dir, mso_build_uid, str(self.simulation_run_uid),
                                                     db_config.host, db_config.user, db_config.password,
                                                     db_config.dse_db_name, db_config.cs_db_name, db_config.port)
        with localconverter(robjects.default_converter + pandas2ri.converter):
            self.design_matrix = robjects.conversion.ri2py(rdf_design_matrix)

        self.design_matrix = spark.createDataFrame(self.design_matrix)

        # Update account IDs to external ID which is UID
        spark_dse_accessor = SparkDSEAccessor()
        account_id_df = spark_dse_accessor.get_account_id_external_id_df()

        self.design_matrix = self.design_matrix.join(account_id_df, on=ACCOUNT_ID_COL, how='left')

        col_labels_to_remove = [ACCOUNT_ID_COL]
        self.design_matrix = self.design_matrix.drop(*col_labels_to_remove)
        self.design_matrix = self.design_matrix.withColumnRenamed('externalId', ACCOUNT_ID_COL)

        """
        NOTE: In current work we are converting the dataframe to spark. Ideally, it should read from the spark context 
        written by the underlying R program.
        """
        # self.design_matrix_spark = spark.createDataFrame(self.design_matrix)

    def __upload_file_to_s3_bucket(self, message_uid, model_file_path):
        """

        :param message_uid:
        :param model_file_path:
        :return:
        """

        logger.info("Uploading model for message uid=" + message_uid)
        # Create session to connect with S3
        session = boto3.Session(
            aws_access_key_id="AKIAIULIODFOMLFJLOQA",
            aws_secret_access_key="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",
        )

        # Upload file to the public S3 bucket
        s3 = session.resource('s3')
        s3_path = "builds/" + self.mso_build_uid + "/" + message_uid
        s3.meta.client.upload_file(Filename=model_file_path, Bucket=S3_BUCKET_NAME, Key=s3_path, ExtraArgs={'ACL':'public-read'})

        # Create full S3 Path and append the row to the data frame
        s3_full_path = "s3://" + S3_BUCKET_NAME + "/" + s3_path
        self.model_s3_path_map = self.model_s3_path_map.append({"messageUID": message_uid, "S3Path": s3_full_path}, ignore_index=True)
        logger.info("Uploaded model at path=" + s3_full_path)


    def __get_model_for_message(self, message_uid):
        """
        This function reads the h2o model stored on the disk for target message
        :param message_uid:
        :return:
        """

        model = None

        if not any(self.model_s3_path_map.messageUID ==  message_uid):

            message_target_name = self.learning_properties.get_property(LearningPropertiesKey.TARGET_TYPE) + "..." + message_uid

            if any(self.model_file_map.target == message_target_name):

                model_file_path = self.model_file_map[self.model_file_map['target'] == message_target_name]['modelName'].values[0]

                if os.path.isfile(model_file_path):
                    self.__upload_file_to_s3_bucket(message_uid, model_file_path)
                else:
                    logger.info("Model file does not exists")

            else:
                logger.info("Model Entry does not exists")

        if any(self.model_s3_path_map.messageUID ==  message_uid):
            model_s3_path = self.model_s3_path_map[self.model_s3_path_map['messageUID'] == message_uid]['S3Path'].values[0]
            print(model_s3_path)
            model = h2o.load_model(model_s3_path)
            logger.info("Model with messageUID = " + message_uid + " found in S3 Bucket at path = " + model_s3_path)

        return model

    def __get_model_variable_list(self, model):
        """
        This function returns the list of variables (i.e. columns from the training data) for the specified model.
        :param model:
        :return:
        """
        assert model, "Invalid Model"

        variable_list = [variable[0] for variable in model.varimp()]

        return variable_list

    def __convert_design_matrix(self, design_matrix, model_col_list):
        """
        This function converts the design matrix to comply the specified model column list.
        :param design_matrix:
        :param model_col_list:
        :return:
        """

        design_matrix_columns = list(design_matrix.col_names)

        columns_to_drop = list(set(design_matrix_columns) - set(model_col_list))

        design_matrix = design_matrix.drop(columns_to_drop, axis=1)

        columns_to_add = list(set(model_col_list) - set(design_matrix_columns))

        for col_to_add in columns_to_add:
            design_matrix[col_to_add] = 0

        return design_matrix

# TO-TEST::
# h2o.init()
# h2o.connect()
#
#
# model = h2o.load_model("/Users/anwar/code/data/72bf6fd6-1533-4072-ad6a-f7aa242ba903/models_72bf6fd6-1533-4072-ad6a-f7aa242ba903/DRF_model_R_1520279502370_1")
# variable_list = [variable[0] for variable in model.varimp()]
# print(variable_list)
#print(MessageSequencePredictor().get_model_variable_list(model))
