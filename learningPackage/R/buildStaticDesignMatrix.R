##########################################################
##
##
## buildDesignMatrix
##
## description: static and dynamic design matrix build for
##              MSO build and score
##
## created by : marc.cohen@aktana.com
## updated by : shirley.xu@aktana.com
##
## created on : 2015-11-03
## updated on : 2018-09-17
##
## Copyright AKTANA (c) 2018.
##
##
##########################################################

#
# build static design matrix
#
buildStaticDesignMatrix <- function(prods, accountProduct, emailTopicNames, logDropProcess=TRUE) {
  
  # constant needed
  AP_MAX_DIMENSION_ALLOWED <- 2^29
  SAFE_LOAD_RATIO <- 1.4
  AP_MAX_DIMENSION_ALLOWED <- AP_MAX_DIMENSION_ALLOWED / SAFE_LOAD_RATIO
  
  predictorNamesAPColMap <- list()
  colsForDcast <- character()
  needFurtherDcastInChunks <- FALSE
  chunkSize <- NA
  # initialize map for predictor variable name in design matrix to original predictor name with colnames in Account/AccountProduct table & [,(,etc (map LoTProxy_akt__2_07_2_49_ to original LoTProxy_akt_(2_07,2_49])
  if(length(prods)==0) { 
    prods <- "accountId"
    # initialize APpredictors for logging the droppend predictors in AccountProduct processing
    APpredictors <- data.frame(namesGroup=character(),dropReason=character(), isNum=numeric(), stringsAsFactors=FALSE)
  } else { 
    # filter out predictors not in accountProduct to avoid crash
    prods <- prods[prods %in% names(accountProduct)]
    # initialize APpredictors for logging the droppend predictors in AccountProduct processing
    APpredictors <- data.frame(namesGroup=prods,dropReason=as.character(NA), isNum=0, stringsAsFactors=FALSE)
    prods <- c("accountId",prods)
  }
  
  # generate features from the clusters of physicalMessageUIDs estimated from the NLP analysis
  flog.info("Processing emailTopics name opens and clicks")
  sM <- accountProduct[,c("accountId",emailTopicNames),with=F]
  # sM$productName <- NULL
  tt <- sM[,colSums(is.na(sM))<nrow(sM)]  # sum up the number of times each account opened/clicked on a specific topic
  sM <- sM[,tt,with=F]                    # pick those columns out
  sM[is.na(sM)] <- 0
  t <- as.data.table(melt(sM,id.vars="accountId"))
  t <- t[value>0]
  t$ctr <- 1
  sM <- as.data.table(dcast(t,accountId~variable+value,fill=0)) # prepare these features for merging into the design
  
  flog.info("Analyzing account product data")
  # prepare the accountProduct table and build design
  # subset accountProduct using predictors
  AP <- accountProduct[,prods,with=F]
  # print(describe(AP))                                         # summarize the accountproduct in the print file for debugging
  
  AP <- AP[,sapply(AP,function(x)length(unique(x))>1),with=F]   # pick out variable withs more than 1 unique value (also drop productName column here)
  if (logDropProcess) {
    colsInAP <- copy(names(AP))                                         # log dropped predictors
    droppedPredictors <- prods[!prods %in% colsInAP]
    flog.info("%s of %s predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique value", length(droppedPredictors), length(prods)-1)
    flog.debug(paste(droppedPredictors, collapse=","))
    if (dim(APpredictors)[1]>0) { # prods configs >0
      APpredictors[APpredictors$namesGroup %in% droppedPredictors, c("dropReason")] <- "<2 unique"
    }
  }
  
  tt<-data.table(sapply(AP,class))                              # find the classes of those variables
  tt$names<-names(AP)
  chrV <- tt[V1=="character"]                                   # pick out the character variables
  chrV <- chrV[names!="productName"]
  numV <- tt[V1!="character"]                                   # pick out the non-character (numeric) variables
  APpredictors[APpredictors$namesGroup %in% numV$names, c('isNum')] <- 1  # log which variable is numeric, so could log where to convert back "_" to for spark dataframe
  
  for(i in chrV$names)                                          # for the numeric variables bucket them based on quantiles
  {                                                             # only do this if there are more than 5 unique values
    eval(parse(text=sprintf("AP[is.na(%s),%s:='NA']",i,i)))
  }
  
  for(i in numV$names[-1])                                      # for the numeric variables bucket them based on quantiles
  {                                                             # only do this if there are more than 5 unique values
    if(i=="accountId")next                                    # skip if it's the accountId
    n <- dim(unique(AP[,i,with=F]))[1]
    if(n>2)                                                   # only do this if there are more than 5 unique values
    {                                                         # could add logic here to make the cut finer - ie more buckets than just the quartiles
      q <- quantile(AP[,i,with=F],probs=seq(0,1,ifelse(n>5,.25,.5)),na.rm=T)                  #  calculate quantiles
      if(sum(q>0)<2){ AP[,(i):=NULL] } else { 
        eval(parse(text=sprintf("AP[,%s:=cut(%s,unique(q),include.lowest=T)]",i,i))) 
        eval(parse(text=sprintf("AP[,%s:=gsub('.', '_', %s, fixed=TRUE)]",i,i)))     # convert name to spark data frame compatible
        }
    } else { eval(parse(text=sprintf("if(length(unique(AP[!is.na(%s)]$%s))<2){ AP$%s<-NULL; } ",i,i,i))) }
  }
  
  if (logDropProcess) {
    droppedPredictors <- colsInAP[!colsInAP %in% names(AP)]       # log dropped predictors
    flog.info("%s of %s numeric predictors (%s all predictors) from AccountProduct/Account dropped before loop to build models as there are either less than 5 unique values or having <2 quantile >0", length(droppedPredictors), dim(numV)[1]-1, length(colsInAP)-1)
    flog.debug(paste(droppedPredictors, collapse=","))
    colsInAP <- copy(names(AP))
    if (dim(APpredictors)[1]>0) { # prods configs >0
      APpredictors[APpredictors$namesGroup %in%droppedPredictors, c("dropReason")] <- "<5 unique numeric or <2 quantile>0"
    }
  }
  
  AP <- AP[,sapply(AP,function(x)length(unique(x))>1),with=F]   # pick out variable withs more than 1 unique value
  if (logDropProcess) {
    droppedPredictors <- colsInAP[!colsInAP %in% names(AP)]       # log dropped predictors
    flog.info("%s of %s remaining predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique values (2nd check after processing numeric and character variable separately", length(droppedPredictors), length(colsInAP)-1)
    flog.debug(paste(droppedPredictors, collapse=","))
    colsInAP <- copy(names(AP))
    if (dim(APpredictors)[1]>0) { # prods configs >0
      APpredictors[APpredictors$namesGroup %in%droppedPredictors, c("dropReason")] <- "<2 unique"
    }
  }
  
  # prepare for later processing (dcast / encode categrorical variable)
  colsForDcast <- names(AP)[names(AP)!="accountId"]

  if (length(colsForDcast) == 0) {  # AP only contains accountId now; no need to furthur process. Return AP
    flog.info("no AP predictors left, use only emailTopic predictors")
    return(list(AP=sM, APpredictors=APpredictors, predictorNamesAPColMap=predictorNamesAPColMap, colsForDcast=colsForDcast, needFurtherDcastInChunks=FALSE, chunkSize=NA))  
  }

  AP[ ,(colsForDcast):=lapply(.SD, as.character), .SDcols=colsForDcast]         # convert AP columns types all to character for melt
  AP[ ,(colsForDcast):=lapply(.SD, function(x){sapply(x, function(y){return(ifelse(is.na(y)|y=="", "missing", y))})}), .SDcols=colsForDcast]                                                                   # convert missing value in AP as missing
  eval(parse(text=sprintf("AP[,`:=`(%s)]", paste(sapply(colsForDcast, function(x){sprintf("%s=paste('%s',%s,sep='_')",x,x,x)}), collapse=","))))                                                # convert value in cols to col_value format
  
  # check if needs to process AP in chunks because of too much unique values it has in a coloumn
  maxUniqueValueCount <- max(sapply(AP[,..colsForDcast],function(x)length(unique(x))))
  needFurtherDcastInChunks <- (maxUniqueValueCount > (AP_MAX_DIMENSION_ALLOWED / nrow(AP)))
  chunkSize <- ifelse(needFurtherDcastInChunks, floor(AP_MAX_DIMENSION_ALLOWED / maxUniqueValueCount), nrow(AP))
  
  # if not needFurtherDcastInChunks, do cast or prepare for later dcast
  if (!needFurtherDcastInChunks) {
    AP[ ,newvalue:=1]                                             # add newvalue 1 to be used for dcast
    AP <- do.call(cbind, c(AP[,c("accountId")], lapply(colsForDcast, function(x){eval(parse(text=sprintf("dcast(AP, accountId~%s, value.var='newvalue', fill=0)[,-c('accountId')]", x)))})))               # do dcast for each column and then cbind
    colsForDcast <- character()
    # log
    if (logDropProcess) { flog.info("After doing dcast, num of predictors in AccountProduct/Account changed from %s to %s", length(colsInAP)-1, dim(AP)[2]-1) }
    
    # remove all special character in AP colnames (predictor variable name), and save the mapping
    originalColnames <- copy(colnames(AP))
    originalColnames <- originalColnames[originalColnames!="accountId"]
    newColnames <- md5(originalColnames)
    predictorNamesAPColMap <- as.list(originalColnames)
    predictorNamesAPColMap <- setNames(predictorNamesAPColMap, as.vector(newColnames))
    setnames(AP, old=as.character(predictorNamesAPColMap), new=names(predictorNamesAPColMap))

  } else {
    # log
    if (logDropProcess) { flog.info("maxUniqueValueCount(%s)*nrow(AP)(%s) exceed AP_MAX_DIMENSION_ALLOWED(%s), delay dcast operations of AP to do in chunks_size(%s) in build & score", maxUniqueValueCount, nrow(AP), AP_MAX_DIMENSION_ALLOWED, chunkSize) }
    
    # compose predictorNamesAPColMap for late ruse to convert column name after dcast to original values
    originalColnames <- unique(unlist(AP[,..colsForDcast]))
    newColnames <- md5(originalColnames)
    predictorNamesAPColMap <- as.list(originalColnames)
    predictorNamesAPColMap <- setNames(predictorNamesAPColMap, as.vector(newColnames))
    AP[ ,(colsForDcast):=lapply(.SD, md5), .SDcols=colsForDcast]         # hash value
  }
  # rm(colsInAP)
  
  ##### clean up memory
  rm(accountProduct,envir=parent.frame())                       # delete the accountProduct table since it's big to free up some memory
  gc()
  ######
  print("after rm accountProduct")                              # next couple of steps just prints out memory use for debugging
  print(sapply(ls(), function(x) as.numeric(object.size(eval(parse(text=x))))))
  #    print(sapply(ls(parent.frame()), function(x) as.numeric(object.size(eval(parse(text=x))))))
  # now add the stable message stuff
  AP<-merge(AP,sM,by="accountId",all.x=T)                       # now merge the messageTopic counts into the AP matrix to complete the build of the design matrix
  flog.info("After adding predictors from EmailTopic Open Rates, final num of predictors in AccountProduct/Account added %s and becomes %s", dim(sM)[2]-1, dim(AP)[2]-1)

  rm(sM)                                                        # done with sM so clean up memory
  
  flog.info("return from buildStaticDesignMatrix")
  return(list(AP=AP, APpredictors=APpredictors, predictorNamesAPColMap=predictorNamesAPColMap, colsForDcast=colsForDcast, needFurtherDcastInChunks=needFurtherDcastInChunks, chunkSize=chunkSize))
}
