

class LearningPropertiesKey:
    """
    This class is responsible to hold all the keys required to read/write learning properties.
    TO-DO: Move this to a common.
    """

    PRODUCT_UID = "productUID"
    BUILD_UID = "buildUID"
    WHITE_LIST_TYPE = "LE_MS_WhiteListType"
    WHITE_LIST = "LE_MS_WhiteList"
    MODEL_TYPE = "modelType"
    TARGET_TYPE = "LE_MS_messageAnalysisTargetType"
    NEW_MESSAGE_STRATEGY = "LE_MS_newMessageStrategy"
    SEGMENTS = "LE_MS_segments"
    VERSION_UID = "versionUID"
    CONFIG_UID = "configUID"
