import unittest
from unittest.mock import patch
import os
import sys

script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
module_dir = os.path.dirname(script_dir)
learning_dir = os.path.dirname(module_dir)
sys.path.append(learning_dir)

from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from simulationMessageSequence.SimulationMessageSequenceDriver import get_copy_storm_database_name
from simulationMessageSequence.SimulationMessageSequenceDriver import SimulationMessageSequenceDriver
from common.DataAccessLayer.DataAccessLayer import SegmentAccessor
from common.DataAccessLayer.DataAccessLayer import MessageAccessor
from common.unitTest.pyunittestUtils import unitTestParent
import simulationMessageSequence.SimulationMessageSequenceScore


_stderr = None
_stdout = None

def suppress_std_output():
    global _stderr
    global _stdout
    _stderr = sys.stderr
    _stdout = sys.stdout
    null = open(os.devnull, 'wb')
    sys.stdout = sys.stderr = null

def unsuppress_std_output():
    sys.stderr = _stderr
    sys.stdout = _stdout

class SimulationDriverTestParent(unitTestParent):

    def setUp(self):
        basepath = os.path.dirname(__file__)
        self.learning_home_dir = os.path.abspath(os.path.join(basepath, "..", ".."))
        self.test_mso_build_uid = "unit-test-mso-build"
        super().setUp()
        self.dbconfig = DatabaseConfig.instance()
        self.dbconfig.set_config(self.db_host, self.db_user, self.db_password, self.db_port, self.dse_db_name, self.learning_db_name, self.cs_db_name, self.stage_db_name)


class MiscFunctionTestCase(SimulationDriverTestParent):

    def test_get_copy_storm_database_name_sample(self):
        self.assertEqual(get_copy_storm_database_name("sample"), "sample_cs", "Invalid copy storm db name for sample")

    def test_get_copy_storm_database_name_pfizer(self):
        self.assertEqual(get_copy_storm_database_name("pfizerusprod"),"pfizerprod_cs", "Invalid copy storm db name for pfizer")

    def test_get_copy_storm_database_name_empty(self):
        self.assertEqual(get_copy_storm_database_name(""), "_cs", "Invalid copy storm db name for empty")

    def reqDBs(self):
        required_dse = [ ]
        required_cs = [ ]
        required_stage = [ ]
        required_learning = [ ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required


class SimulationDriverTestCase(SimulationDriverTestParent):

    def __is_valid_simulation_run_uid(self, sim_run_uid):

        is_valid_sim_run_uid = True
        return is_valid_sim_run_uid

    def test_init_empty_values(self):
        self.assertRaises(AssertionError, SimulationMessageSequenceDriver, "", "")

    def test_init_invalid_learning_dir(self):
        self.assertRaises(AssertionError, SimulationMessageSequenceDriver, "/dsfd/ddd/@@@444/##", "")

    def test_init_invalid_mso_build_uid(self):
        self.assertRaises(AssertionError, SimulationMessageSequenceDriver, "./simulationMessageSequence/test/data", "")

    def test_init_valid_input(self):
        self.simulation_test_data = os.path.join(self.learning_home_dir, "simulationMessageSequence", "tests", "data")

        simulation_driver = SimulationMessageSequenceDriver(self.simulation_test_data, self.test_mso_build_uid)
        # Check if MSO directory
        self.assertEqual(simulation_driver.mso_build_dir, self.simulation_test_data + "/builds/" + self.test_mso_build_uid, "Incorrect MSO build directory")

        # Check learning home directory
        self.assertEqual(simulation_driver.learning_home_dir, self.simulation_test_data, "Incorrect learning home directory")

        # Check data base entry for learningRunUID
        sim_run_uid = simulation_driver.simulation_run_uid
        self.assertTrue(self.__is_valid_simulation_run_uid(sim_run_uid), "Simulation RunUID is invalid.")

    def reqDBs(self):
        required_dse = [ ]
        required_cs = [ ]
        required_stage = [ ]
        required_learning = [ "LearningRun", "SimulationMessageSequence", "SimulationSegment", "SimulationAccountSegment", "SimulationAccountMessageSequence", "SimulationAccountSentEmail", "SimulationSegmentMessageSequence", "h2oDockerInfo", "Sent_Email_vod__c_arc" ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required


class SimulationProcessTester(SimulationDriverTestParent):

    def setUp(self):
        super().setUp()
        self.simulation_test_data = os.path.join(self.learning_home_dir, "simulationMessageSequence", "tests", "data")
        self.simulation_driver = SimulationMessageSequenceDriver(self.simulation_test_data, self.test_mso_build_uid)
        self.sim_run_uid = self.simulation_driver.simulation_run_uid

    def test_generate_segments(self):

        #function inputs
        #expected outputs
        SEGMENT_COUNT = 21
        SEGMENT_ACCOUNT_COUNT = 620

        #testing outputs
        self.simulation_driver.generate_segments_v2()

        segment_accessor = SegmentAccessor()
        generated_segment_count = segment_accessor.get_segment_count(self.test_mso_build_uid, self.sim_run_uid)

        self.assertEqual(generated_segment_count, SEGMENT_COUNT)

        generated_segment_account_count = segment_accessor.get_segment_account_count(self.test_mso_build_uid, self.sim_run_uid)
        self.assertEqual(generated_segment_account_count, SEGMENT_ACCOUNT_COUNT)

    @patch("simulationMessageSequence.SimulationMessageSequenceDriver.MessageSequencePredictor")
    @patch("simulationMessageSequence.SimulationMessageSequenceDriver.SimulationScoreAggregator")
    @patch("simulationMessageSequence.SimulationMessageSequenceDriver.SimulationMessageSequenceDriver.perform_simulation_clean_up")
    def test_simulation(self, mock_perform_simulation_clean_up, mock_SimulationScoreAggregator, mock_MessageSequencePredictor):

        #function inputs
        #expected outputs

        #testing outputs
        self.simulation_driver.start_simulation()
        mock_MessageSequencePredictor.assert_called()
        mock_SimulationScoreAggregator.assert_called()
        mock_perform_simulation_clean_up.assert_called()

        #tests from the original unit test - determined to be out of the scope of this unit test
        #keeping as reference for writing future unit tests
        # SEGMENT_COUNT = 21
        # SEGMENT_ACCOUNT_COUNT = 620
        # SIM_MSG_SEQ_COUNT = 144
        # SIM_ACCOUNT_MSG_SEQ_COUNT = 310
        # SIM_SEG_MSG_SEQ_COUNT = 21
        # segment_accessor = SegmentAccessor()
        # generated_segment_count = segment_accessor.get_segment_count(self.test_mso_build_uid, self.sim_run_uid)
        # self.assertEqual(generated_segment_count, SEGMENT_COUNT)
        # generated_segment_account_count = segment_accessor.get_segment_account_count(self.test_mso_build_uid,
        #                                                                              self.sim_run_uid)
        # self.assertEqual(generated_segment_account_count, SEGMENT_ACCOUNT_COUNT)
        # message_accessor = MessageAccessor()
        # db_message_seq_count = message_accessor.get_message_seq_count(self.test_mso_build_uid, self.sim_run_uid)
        # self.assertEqual(db_message_seq_count, SIM_MSG_SEQ_COUNT)
        # db_account_message_seq_count = message_accessor.get_account_message_seq_count(self.test_mso_build_uid,
        #                                                                              self.sim_run_uid)
        # self.assertEqual(db_account_message_seq_count, SIM_ACCOUNT_MSG_SEQ_COUNT)
        # db_segment_message_seq_count = message_accessor.get_segment_message_seq_count(self.test_mso_build_uid,
        #                                                                               self.sim_run_uid)
        # self.assertEqual(db_segment_message_seq_count, SIM_SEG_MSG_SEQ_COUNT)

    def reqDBs(self):
        required_dse = [ "Account" ]
        required_cs = [ ]
        required_stage = [ ]
        required_learning = [ "LearningRun", "SimulationMessageSequence", "SimulationSegment", "SimulationAccountSegment", "SimulationAccountMessageSequence", "SimulationAccountSentEmail", "SimulationSegmentMessageSequence", "h2oDockerInfo", "Sent_Email_vod__c_arc" ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required
