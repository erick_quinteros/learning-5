homedir <- '~/Documents/learning'

# customize RepAccountAssignment_arc for new rep
RepAccountAssignment_arc <- read.csv(file=sprintf('%s/anchor/tests/data/pfizerusdev_stage/RepAccountAssignment_arc.csv', homedir), header=TRUE, sep=",", as.is=TRUE)
temp <- RepAccountAssignment_arc[RepAccountAssignment_arc$repId==1505,]
temp$repId <- 1623
RepAccountAssignment_arc <- rbind(RepAccountAssignment_arc,temp)
write.csv(RepAccountAssignment_arc, file=sprintf('%s/anchor/tests/data/pfizerusdev_stage/RepAccountAssignment_arc.csv', homedir), row.names=FALSE)

# customize interaction and interactionAccount
interactions <- read.csv(file=sprintf('%s/anchor/tests/data/pfizerusdev/Interaction.csv', homedir), header=TRUE, sep=",", as.is=TRUE)
temp <- interactions[interactions$repId==1505,]
temp <- temp[temp$isCompleted==0 | temp$interactionId %in% c(146946011,206590788),]
pickInts <- temp$interactionId
temp$repId <- 1623
temp$interactionId <- temp$interactionId + 100000000
temp$externalId <- paste(temp$externalId,'X',sep="")
interactions <- rbind(interactions, temp)
write.csv(interactions, file=sprintf('%s/anchor/tests/data/pfizerusdev/Interaction.csv', homedir), row.names=FALSE)
# interactionAccount
interactionAccount <- read.csv(file=sprintf('%s/anchor/tests/data/pfizerusdev/InteractionAccount.csv', homedir), header=TRUE, sep=",", as.is=TRUE)
temp <- interactionAccount[interactionAccount$interactionId %in% pickInts,]
temp$interactionId <- temp$interactionId + 100000000
interactionAccount <- rbind(interactionAccount, temp)
write.csv(interactionAccount, file=sprintf('%s/anchor/tests/data/pfizerusdev/InteractionAccount.csv', homedir), row.names=FALSE)