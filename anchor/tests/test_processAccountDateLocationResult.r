context('testing processAccountDateLocationResult script in ANCHOR module')
print(Sys.time())

library(Learning)
library(properties)

# load library and source script
source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))
source(sprintf("%s/anchor/code/processAccountDateLocationResult.r",homedir))
source(sprintf("%s/anchor/code/utils.r",homedir))

# load required input
load(sprintf('%s/anchor/tests/data/from_calculateAccountDateLocation_result.RData', homedir))
buildUID <- readModuleConfig(homedir, 'anchor','buildUID')
config <- read.properties(sprintf('%s/anchor/tests/data/%s/learning.properties',homedir,buildUID))
# nightly and manual predictionRunDate are same in learning.properties. will need update if this changes
predictRundate <- getConfigurationValueNew(config,"LE_AN_manualPredictRundate",convertFunc=as.Date)
predictAhead <- getConfigurationValueNew(config,"LE_AN_predictAhead",convertFunc=as.integer) # predict how many days in the future
predictAheadDayList <- utils.generatePredictAheadDayList(predictRundate, predictAhead)

# dont test for direct equality in accountDateLocation DSE result because the order different facilities groped by accountId,date with equal score are piced 
# instead, make sure there is an entry for every accountId,date pair there should be
test_that("test processed accountDateLocation data is the same as the one saved", {
  # test function
  accountDateLocation_processed_new <- processAccountDateLocationResult(accountDateLocation, predictAheadDayList, predictRundate)
  # check dimension is correct
  expect_equal(dim(accountDateLocation_processed_new),c(1861,7))
  # check same as saved
  load(sprintf('%s/anchor/tests/data/from_processAccountDateLocationResult.RData',homedir))
  #accountDateLocation <- accountDateLocation_processed_new
  #save(accountDateLocation, file=sprintf('%s/anchor/tests/data/from_processAccountDateLocationResult.RData',homedir))
  expect_equal(accountDateLocation_processed_new[order(accountId,date,facilityId),],accountDateLocation[order(accountId,date,facilityId),])
})







