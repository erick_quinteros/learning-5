##########################################################
#
#
# aktana- anchor location estimates Aktana Learning Engines.
#
# description: calculate an account location prediction given interaction data
#
# created by : jacob.adicoff@aktana.com
# updated by : jacob.adicoff@aktana.com
#
# created on : 2018-11-01
# updated on : 2018-11-13
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################


library(data.table)
library(futile.logger)


calculateAccountDateLocationScores <- function(interactions) {
  
  flog.info(sprintf('Start calculateAccountDateLocationScores: with %s rows of interaction data', dim(interactions)[[1]]))
  
  # explicitly set startdate as UTC time, then get local time
  interactions[,startDateTime:=as.POSIXct(startDateTime, format="%Y-%m-%d %H:%M:%S", tz='UTC')]
  flog.debug("finish convert startTime to datetime object")
  interactions[,startDateTimeLocal:=format(startDateTime, format="%Y-%m-%d %H:%M:%S", tz=timeZoneId, usetz=TRUE), by=1:nrow(interactions)]
  flog.debug("finish processing startTimeLocal")
  
  # calcualte periodOfDay & filter out interactions
  interactions[,periodOfDay:=floor(as.integer(substr(startDateTimeLocal,12,13))+as.numeric(substr(startDateTimeLocal,15,16))/60-.5)]
  interactions[,periodOfDay:=ifelse((periodOfDay > 9 & periodOfDay<= 12), "morning", ifelse(periodOfDay <= 14, "afternoon", ifelse(periodOfDay <= 17 ,"evening", "none")))]
  flog.debug("finish processing of periodOfDay & filter interactions out of valid visit time frame, nrow(interactions)=(%s)", nrow(interactions))
  
  # build an hourly time of day within workday hours
  interactions[,dayOfWeek:=weekdays(as.Date(startDateTimeLocal,format="%Y-%m-%d %H:%M:%S",tz=timeZoneId)), by=1:nrow(interactions)]
  flog.debug("finish processing of dayOfWeek")

  # count rows
  # get number of rows in grouping, save in facilityDayPeriodInteractions. Additionally nulls unused cols
  getRowCountByGroup <- function(latitude, longitude) {
    return (list(latitude=latitude[1], longitude=longitude[1], facilityDayPeriodInteractions=length(latitude) ))
  }
  accountDateLocationScores <- interactions[,getRowCountByGroup(latitude,longitude),by=c("accountId","facilityId","periodOfDay","dayOfWeek")]
  flog.debug("finish counting num of interactions by accountId, facilityId, DoW, PoD")
  
  accountDateLocationScores[,c("totalInteractions","totalFilteredInteractions"):=.(sum(facilityDayPeriodInteractions),sum(facilityDayPeriodInteractions[periodOfDay!='none'])),by="accountId"]
  flog.debug("finish counting num of interactions by accountId")
  
  # comput DoW prob & interaction count
  accountDateLocationScores[,c("facilityDayInteractions"):=sum(facilityDayPeriodInteractions),by=c("accountId","facilityId","dayOfWeek")]
  flog.debug("finish sum PoD num of interactions to get DoW prob & num of interactions")
  
  # compute PoD prob
  accountDateLocationScores[,facilityDayProbability:=facilityDayInteractions/totalInteractions]
  accountDateLocationScores[,facilityDayPeriodProbability:=ifelse(periodOfDay == 'none', 0, facilityDayPeriodInteractions/totalFilteredInteractions)]
  flog.debug("finish calucalting PoD prob")
  
  # calculate confidence
  unqN_DoW <- unique(accountDateLocationScores[,list(accountId,facilityDayInteractions)])
  tot_DoW <- sum(unqN_DoW$facilityDayInteractions)
  unqN_PoD <- unique(accountDateLocationScores[,list(accountId,facilityDayPeriodInteractions)])
  tot_PoD <- sum(unqN_PoD$facilityDayPeriodInteractions)
  accountDateLocationScores[,c("confidenceDoW","confidencePoD"):=.(scale(totalInteractions/tot_DoW, center=F),scale(totalFilteredInteractions/tot_PoD, center=F))]
  accountDateLocationScores[,c("confidenceDoW","confidencePoD"):=.(confidenceDoW/max(confidenceDoW),confidencePoD/max(confidencePoD))]
  flog.debug("finish calculation of confidence")

  # compute scores by multiple to confidence
  accountDateLocationScores[,c("facilityDoWScore","facilityPoDScore"):=.(confidenceDoW*facilityDayProbability, confidencePoD*facilityDayPeriodProbability)]
  # accountDateLocationScores[,facilityPoDScore:=confidence*facilityDayPeriodProbability]
  flog.debug("finish calculation of scores by confidence*prob")
  # drop column not needed
  accountDateLocationScores[,c("facilityDayProbability","facilityDayPeriodProbability","facilityDayInteractions","facilityDayPeriodInteractions","totalInteractions","totalFilteredInteractions","confidenceDoW","confidencePoD"):=NULL]
  
  flog.info(sprintf('Return from calculateAccountDateLocationScores: dim(accountDateLocationScores)=(%s)',  paste(dim(accountDateLocationScores),collapse=",")))		
  return(accountDateLocationScores)
  
}


calculateAccountDateLocation <- function(accountDateLocationScores, fixOutput) {
  # if also a weekly run, drop cols that dse does not use. Note to change in dataLoad when switching to period of day output
  flog.info(sprintf('Start calculateAccountDateLocation: using %s rows of AccountDateLocationScores data', dim(accountDateLocationScores)[[1]]))		
  # ensure fixed output for automation testing
  if(fixOutput) setorder(accountDateLocationScores, accountId, facilityId, periodOfDay, dayOfWeek)
  
  # get max score with accountId/DoW grouping, random tie breaks
  randomMaxFromGroup <- function(facilityId,latitude,longitude,facilityDoWScore) {
    ind <- which(facilityDoWScore==max(facilityDoWScore))
    randomIndexInGroup <- ifelse(length(ind)>1, sample(ind, 1), ind)
    return (list(latitude=latitude[randomIndexInGroup], longitude=longitude[randomIndexInGroup], facilityId=facilityId[randomIndexInGroup], facilityDoWScore=facilityDoWScore[randomIndexInGroup]))
  }
  accountDateLocation <- accountDateLocationScores[,randomMaxFromGroup(facilityId,latitude,longitude,facilityDoWScore),by=c("accountId","dayOfWeek")]
  flog.info(sprintf('Return from calculateAccountDateLocation: dim(accountDateLocation)=(%s)', paste(dim(accountDateLocation),collapse=",")))	
  
  return(accountDateLocation)
}
