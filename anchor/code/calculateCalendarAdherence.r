##########################################################
#
#
# aktana- anchor location estimates Aktana Learning Engines.
#
# description: calculate calendar adherence
#
# created by : learning-dev@aktana.com
# updated by : learning-dev@aktana.com
#
# created on : 2019-06-20
# updated on : 2019-06-20
#
# Copyright AKTANA (c) 2019.
#
#
####################################################################################################################

library(data.table)
library(futile.logger)
library(reticulate)

calculateCalendarAdherence <- function(callHistory) {
  
  flog.info("start calculating calendar adherence")
  
  CONFIDENCE_LEVEL <- 0.05
  
  # source python script
  use_python("/usr/bin/python3")
  use_python("/Users/shirleyxu/anaconda/bin/python3")
  source_python(sprintf("%s/anchor/code/calculate_calendar_adherence.py",homedir))
  
  # set default
  repCalendarAdherence <- data.table()
  repAccountCalendarAdherence <- data.table()
  
  # call python function to run calculating calendar adherence
  flog.info("Counting complete & reschedule for callHistory...")
  calendarAdherenceCount <- count_complete_reschedule(callHistory)
  
  flog.info("Agg complete & reschedule count for rep account comb")
  calendarAdherenceCount_rep_account <- agg_complete_reschedule_count(calendarAdherenceCount, c('repId','accountId'))
  flog.info("Counting complete & reschedule for rep")
  calendarAdherenceCount_rep <- agg_complete_reschedule_count(calendarAdherenceCount_rep_account, 'repId')

  flog.info("Processing calendar adherence for rep")
  repCalendarAdherence <- data.table(calculate_calendar_adherence(calendarAdherenceCount_rep, CONFIDENCE_LEVEL))
  flog.info("Processing calendar adherence for rep account comb")
  repAccountCalendarAdherence <- data.table(calculate_calendar_adherence(calendarAdherenceCount_rep_account, CONFIDENCE_LEVEL))
  
  flog.info(sprintf('Return from calculateCalendarAdherence: dim(repCalendarAdherence)=(%s), dim(repAccountCalendarAdherence)=(%s)', paste(dim(repCalendarAdherence),collapse=","), paste(dim(repAccountCalendarAdherence),collapse=",")))	
  
  return(list(repCalendarAdherence=repCalendarAdherence, repAccountCalendarAdherence=repAccountCalendarAdherence))
}