context('test the messageScoreDriver script in the spark MSO module')
print(Sys.time())

# load library, loading common source scripts
library(uuid)
library(openxlsx)
library(Learning)
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
library(sparkLearning)

# build used for testing
buildUID <- readModuleConfig(homedir, 'messageSequence','buildUID')
added_buildUID <- readModuleConfig(homedir, 'messageSequence','buildUID_nightlyscoreadd')

######################## func for reset mock data for run messageScoreDriver ###############################
runMessageScoreDriverMockDataReset <- function(isNightly, buildUID) {
  # writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  requiredMockDataList <- list(pfizerusdev=c('Account','Product','AccountProduct','AccountMessageSequence','Message','MessageSet','MessageAlgorithm','RepTeam','MessageSetMessage','Event','EventType','Interaction','InteractionType','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','Rep','RepTeamRep','RepAccountAssignment'),pfizerusdev_learning=c('LearningObjectList','AccountMessageSequence','MessageRescoringTimes','LearningRun','LearningBuild','AKT_Message_Topic_Email_Learned','Sent_Email_vod__c_arc'),pfizerprod_cs=c('Approved_Document_vod__c'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)

  # add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  setupMockBuildDir(homedir, 'messageSequence', buildUID, forNightly=isNightly)

  # copy the build folder using new BUILD_UID to make sure the loop algorithm of nightly scoring works
  if (isNightly) {
    new_buildUID <- readModuleConfig(homedir, 'messageSequence','buildUID_nightlyscoreadd')
    new_versionUID <- readModuleConfig(homedir, 'messageSequence','versionUID_nightlyscoreadd')
    new_configUID <- readModuleConfig(homedir, 'messageSequence','configUID_nightlyscoreadd')
    setupMockBuildDir (homedir, 'messageSequence', buildUID, files_to_copy=NULL, forNightly=TRUE, new_BUILD_UID=new_buildUID, new_configUID=new_configUID, new_versionUID=new_versionUID)
  }
}

######################## func for run messageScoreDriver ##########################################
runMessageScoreDriver <- function(isNightly, buildUID) {
  flog.appender(appender.console())
  suppressWarnings(sink(NULL))
  # fix scoreDate so that the result is always the same
  scoreDate <<- as.Date(readModuleConfig(homedir, 'messageSequence','scoreDate'))
  if (!isNightly) {
    # arguments needed for manual scoring
    RUN_UID <<- UUIDgenerate()
    BUILD_UID <<- buildUID
    # add entry to learningRun (simulate what done by API before calling the R script) (for manual scoring only)
    config <- initializeConfigurationNew(homedir, BUILD_UID)
    VERSION_UID <- config[['versionUID']]
    CONFIG_UID <- config[['configUID']]
    con <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    SQL <- sprintf("INSERT INTO LearningRun (learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',0,'MSO','running','%s');",RUN_UID, BUILD_UID, VERSION_UID, CONFIG_UID, now)
    dbClearResult(dbSendQuery(con,SQL))
    dbDisconnect(con)
  } else {
    if (exists("BUILD_UID", envir=globalenv())) {rm(BUILD_UID, envir=globalenv())}
    if (exists("RUN_UID", envir=globalenv())) {rm(RUN_UID, envir=globalenv())}
    if (exists("messageRescoringTimes", envir=globalenv())) {rm(messageRescoringTimes, envir=globalenv())} # messageRescoringTimes evaluate to global and will not be loaded from DB again if already exists
  }
  # run messageScoreDriver
  sc <<- initializeSpark(homedir)
  source(sprintf('%s/sparkMessageSequence/messageScoreDriver.r',homedir))
  return (RUN_UID)
}

############################# main test script ########################################################
# test cases
test_that('test messageScoreDriver for nighlty scoring', {
  # run script
  isNightly <- TRUE
  runMessageScoreDriverMockDataReset(isNightly, buildUID)
  numOfFilesInBuildFolder <- checkNumOfFilesInFolder(sprintf('%s/builds/%s',homedir,buildUID))
  runUID <- runMessageScoreDriver(isNightly, buildUID)

  # test cases
  # load saved nightly scoring for comparison
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_nightly_scores.RData', homedir))
  savedScores <- setkey(scores,NULL)

  # test entry in DB is fine
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')
  learningBuild <- dbGetQuery(con_l, 'SELECT * from LearningBuild;')
  messageeRescoringTimes <- dbGetQuery(con_l, 'SELECT * from MessageRescoringTimes;')
  accountMessageSequence <- dbGetQuery(con, 'SELECT * from AccountMessageSequence;')
  dbDisconnect(con)
  dbDisconnect(con_l)
  expect_equal(dim(learningBuild), c(0,9))
  expect_equal(dim(learningRun), c(2,10))
  expect_setequal(unname(unlist(learningRun[,c('learningBuildUID','isPublished','runType','executionStatus')])), c(buildUID,added_buildUID,1,1,'MSO','MSO','success','success'))
  # get runUID
  runUID <- learningRun[learningRun$learningBuildUID==buildUID,"learningRunUID"]
  runUID2 <- learningRun[learningRun$learningBuildUID==added_buildUID,"learningRunUID"]
  # test save resutls in DB
  expect_equal(dim(accountMessageSequence), c(368,9))
  expect_length(unique(accountMessageSequence$messageAlgorithmId), 2)
  expect_equal(dim(accountMessageSequence[accountMessageSequence$learningRunUID==runUID,]), c(184,9))
  expect_equal(dim(accountMessageSequence[accountMessageSequence$learningRunUID==runUID2,]), c(184,9))
  expect_equal(dim(messageeRescoringTimes), c(202,7))
  expect_equal(dim(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==buildUID,]), c(96,7))
  expect_equal(dim(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==added_buildUID,]), c(96,7))
  expect_equal(unique(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==buildUID,c("physicalMessageUID")]), "a3RA00000001MtAMAU")
  expect_equal(dim(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==buildUID & messageeRescoringTimes$rescoringTimes==1,]),c(96,7))
  # test the saved scores in DB is the same as the one saved locally
  savedScores$probability <- savedScores$prob*savedScores$AUC
  savedScores$predictDB <- 1
  expect_equal(unname(accountMessageSequence[accountMessageSequence$learningRunUID==runUID,c("accountId","probability","predict","messageId")]), unname(data.frame(savedScores[,c("accountId","probability","predictDB","msgId")])))

  # check build dir structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_num_of_file(sprintf('%s/builds/%s',homedir,buildUID), numOfFilesInBuildFolder+4)
  expect_file_exists(sprintf('%s/builds/%s/plot_output_%s',homedir,buildUID,paste("Score",runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/messageScoreDriver.r_%s_%s.xlsx',homedir,buildUID,buildUID,runUID))
  expect_file_not_exists(sprintf('%s/builds/%s/Design.RData',homedir,buildUID))

  # check scores saved in messageScoreDriver.xlsx is correct
  savedExcelScores <- data.table(read.xlsx(sprintf('%s/builds/%s/messageScoreDriver.r_%s_%s.xlsx',homedir,buildUID,buildUID,runUID), sheet=2))
  expect_equal(savedExcelScores, unique(savedScores[,c("messageId","aveRate","breakProb")]))

  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))
  # read config for result comparison
  propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,buildUID)
  config <- read.properties(propertiesFilePath)
  # check general log information
  ind <- grep('Score buildUID:',log_contents)
  expect_str_end_match(log_contents[ind],buildUID)
  ind <- grep('Analyzing product:',log_contents)
  expect_str_end_match(log_contents[ind],'CHANTIX')
  # check rescoring parameter
  ind <- grep('Runing scoring job with', log_contents)
  expect_str_pattern_find(log_contents[ind], paste("flagRescoring=",config[["LE_MS_flagRescoring"]],sep=""))
  ind <- sum(grepl('RescorinTimes Limit is 2', log_contents))
  expect_equal(ind>=1, TRUE)
  # check message a3RA00000001MtAMAU
  ind <- grep('OPEN___a3RA00000001MtAMAU',log_contents)
  expect_length(ind,2) # have start analysis and analysis finish successfully
  ind <- ind[1]
  expect_str_end_match(log_contents[ind+1],'703') # msgId
  expect_str_end_match(log_contents[ind+2],'309') # before rm sent+target records to score
  expect_str_pattern_find(log_contents[ind+5], "96") # number of accounts to rescore
  expect_str_end_match(log_contents[ind+6],'184') # num of reocrds to score after rm and plus rescoring
  # check message a3RA0000000e0u5MAA
  ind <- grep('OPEN___a3RA0000000e486MAA',log_contents)
  expect_length(ind,1) # as this is expired message, so skip scoring half way
  expect_str_pattern_find(log_contents[ind+1],'expired') # detect msg is expired
  # check random sampling
  ind <- grep('Messaging strategy:',log_contents)
  expect_str_end_match(log_contents[ind],config[["LE_MS_newMessageStrategy"]])
  # check message a3RA0000000e0wBMAQ
  ind <- sum(grepl('a3RA0000000e0wBMAQ',log_contents))
  expect_equal(ind,0) # this message is not in message set, so will not be scoree this msg
  # check whether random sample calcuation is correct
  ind <- grep('a3RA0000000e47gMAA',log_contents)
  expect_str_end_match(log_contents[ind[1]],'lower bound 0.000845584853835724 upper bound 0.0780355582939658') # check calculation
  expect_str_pattern_find(log_contents[ind[1]+1],'expired') # check detected as expired
})


test_that('test messageScoreDriver for nighlty scoring again with the same data and without cleaning DB', {
  # setup
  isNightly <- TRUE
  runMessageScoreDriverMockDataReset(isNightly, buildUID)
  numOfFilesInBuildFolder <- checkNumOfFilesInFolder(sprintf('%s/builds/%s',homedir,buildUID))

  # update to not score for messageAlorithmId =25
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  dbClearResult(dbSendQuery(con, 'UPDATE MessageAlgorithm SET isActive=0 WHERE messageAlgorithmId=25;'))
  # update messageRescoringTimes with previous run data
  load(sprintf('%s/messageSequence/tests/data/from_messageScoreDriver_messageeRescoringTimes_nightly_1st.RData', homedir))
  dbClearResult(dbSendQuery(con_l, 'TRUNCATE MessageRescoringTimes;'))
  dbWriteTable(con_l, "MessageRescoringTimes", as.data.frame(messageeRescoringTimes), overwrite=FALSE, append=TRUE, row.names=FALSE)
  # update AccountMessageSequence with previous run data
  load(file=sprintf('%s/messageSequence/tests/data/from_messageScoreDriver_accountMessageSequence_nightly_1st.RData', homedir))
  dbClearResult(dbSendQuery(con, 'TRUNCATE AccountMessageSequence;'))
  dbWriteTable(con, "AccountMessageSequence", as.data.frame(accountMessageSequence), overwrite=FALSE, append=TRUE, row.names=FALSE)

  # run script
  runUID <- runMessageScoreDriver(isNightly, buildUID)

  # test cases
  # check build dir structure
  expect_num_of_file(sprintf('%s/builds/%s',homedir,buildUID), numOfFilesInBuildFolder+4)
  # check DB entry
  messageeRescoringTimes <- dbGetQuery(con_l, 'SELECT * from MessageRescoringTimes;')
  accountMessageSequence <- dbGetQuery(con, 'SELECT * from AccountMessageSequence;')
  dbDisconnect(con)
  dbDisconnect(con_l)
  expect_equal(dim(accountMessageSequence), c(184,9))
  expect_equal(dim(messageeRescoringTimes), c(202,7))
  expect_equal(dim(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==added_buildUID & messageeRescoringTimes$rescoringTimes==1,]),c(96,7))
  expect_equal(dim(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==added_buildUID & messageeRescoringTimes$rescoringTimes==2,]),c(0,7))
  expect_equal(dim(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==buildUID & messageeRescoringTimes$rescoringTimes==1,]),c(0,7))
  expect_equal(dim(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==buildUID & messageeRescoringTimes$rescoringTimes==2,]),c(96,7))
})


test_that('test messageScoreDriver for nighlty scoring again (3rd time) with the same data and without cleaning DB (should not rescoring as reach rescoring limit)', {
  # setup
  isNightly <- TRUE
  runMessageScoreDriverMockDataReset(isNightly, buildUID)
  numOfFilesInBuildFolder <- checkNumOfFilesInFolder(sprintf('%s/builds/%s',homedir,buildUID))

  # update to not score for messageAlorithmId =25
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  dbClearResult(dbSendQuery(con, 'UPDATE MessageAlgorithm SET isActive=0 WHERE messageAlgorithmId=25;'))
  # update messageRescoringTimes with previous run data
  load(sprintf('%s/messageSequence/tests/data/from_messageScoreDriver_messageeRescoringTimes_nightly_2nd.RData', homedir))
  dbClearResult(dbSendQuery(con_l, 'TRUNCATE MessageRescoringTimes;'))
  dbWriteTable(con_l, "MessageRescoringTimes", as.data.frame(messageeRescoringTimes), overwrite=FALSE, append=TRUE, row.names=FALSE)

  # run script
  runUID <- runMessageScoreDriver(isNightly, buildUID)

  # test cases
  # check build dir structure
  expect_num_of_file(sprintf('%s/builds/%s',homedir,buildUID), numOfFilesInBuildFolder+4)
  # check DB entry
  accountMessageSequence <- dbGetQuery(con, 'SELECT * from AccountMessageSequence;')
  messageeRescoringTimes <- dbGetQuery(con_l, 'SELECT * from MessageRescoringTimes;')
  dbDisconnect(con_l)
  dbDisconnect(con)
  expect_equal(dim(accountMessageSequence), c(88,9))
  expect_equal(dim(messageeRescoringTimes), c(202,7))
  expect_equal(dim(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==buildUID & messageeRescoringTimes$rescoringTimes==1,]),c(0,7))
  expect_equal(dim(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==buildUID & messageeRescoringTimes$rescoringTimes==2,]),c(96,7))
  expect_equal(dim(messageeRescoringTimes[messageeRescoringTimes$learningBuildUID==buildUID & messageeRescoringTimes$rescoringTimes==3,]),c(0,7))
})


test_that('test messageScoreDriver for manual scoring', {
  # run script
  isNightly <- FALSE
  runMessageScoreDriverMockDataReset(isNightly, buildUID)
  runUID <- runMessageScoreDriver(isNightly, buildUID)

  # test cases
  # load saved manual scoring data for comparison
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_manual_scores.RData', homedir))
  savedScores <- setkey(scores,NULL)

  # test entry in DB is fine
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')
  accountMessageSequence_l <- dbGetQuery(con_l, 'SELECT * from AccountMessageSequence;')
  accountMessageSequence <- dbGetQuery(con, 'SELECT * from AccountMessageSequence;')
  dbDisconnect(con)
  dbDisconnect(con_l)
  expect_equal(dim(learningRun), c(1,10))
  expect_equal(unname(unlist(learningRun[,c('learningRunUID','learningBuildUID','isPublished','runType','executionStatus')])), c(runUID,buildUID,0,'MSO','success'))
  expect_equal(learningRun$updatedAt>=learningRun$createdAt, TRUE)
  expect_equal(dim(accountMessageSequence_l), c(13993,7))
  expect_equal(dim(accountMessageSequence), c(0,9))

  # check build dir structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_file_exists(sprintf('%s/builds/%s/log_%s.txt',homedir,buildUID,paste("Score",runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/print_%s.txt',homedir,buildUID,paste("Score",runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/plot_output_%s',homedir,buildUID,paste("Score",runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/messageScoreDriver.r_%s_%s.xlsx',homedir,buildUID,buildUID,runUID))
  expect_file_exists(sprintf('%s/builds/%s/Design.RData',homedir,buildUID))

  # check scores saved in messageScoreDriver.xlsx is correct
  savedExcelScores <- data.table(read.xlsx(sprintf('%s/builds/%s/messageScoreDriver.r_%s_%s.xlsx',homedir,buildUID,buildUID,runUID), sheet=2))
  expect_equal(savedExcelScores[savedExcelScores$messageId %in% savedScores$messageId,], unique(savedScores[,c("messageId","aveRate","breakProb")]))

  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))
  # check general log information
  ind <- grep('Score buildUID:',log_contents)
  expect_str_end_match(log_contents[ind],buildUID)
  ind <- grep('Analyzing product:',log_contents)
  expect_str_end_match(log_contents[ind],'CHANTIX')
  # check isNightly flag
  ind <- sum(grepl('isNightly=FALSE', log_contents))
  expect_equal(ind>=1, TRUE)
  # check message a3RA00000001MtAMAU
  ind <- grep('OPEN___a3RA00000001MtAMAU',log_contents)
  expect_length(ind,2) # have start analysis and analysis finish successfully
  ind <- ind[1]
  expect_str_end_match(log_contents[ind+1],'703') # msgId
  expect_str_end_match(log_contents[ind+2],'309') # before rm sent+target records to score
  expect_str_end_match(log_contents[ind+4],'88') # num of reocrds to score after rm
  # check message a3RA0000000e0u5MAA
  ind <- grep('OPEN___a3RA0000000e486MAA',log_contents)
  expect_length(ind,1) # as this is expired message, so skip scoring half way
  expect_str_pattern_find(log_contents[ind+1],'expired') # detect msg is expired
  # check random sampling
  ind <- grep('Messaging strategy:',log_contents)
  propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,buildUID)
  config <- read.properties(propertiesFilePath)
  expect_str_end_match(log_contents[ind],config[["LE_MS_newMessageStrategy"]])
  # check message a3RA0000000e0wBMAQ
  ind <- grep('a3RA0000000e0wBMAQ',log_contents)
  expect_equal(ind,integer(0)) # this message is from event table, so looping from Messages will not score this msg
  # check whether random sample calcuation is correct
  ind <- grep('a3RA0000000e483MAA',log_contents)
  expect_str_end_match(log_contents[ind[1]],'lower bound 0.000845584853835724 upper bound 0.0856545222273745') # check calculation
  expect_str_pattern_find(log_contents[ind[1]+1],'expired') # check detected as expired
})
