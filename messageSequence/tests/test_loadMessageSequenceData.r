context('testing loadMessageSequenceData() func in MSO module')
print(Sys.time())

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list(pfizerusdev=c('Product','RepTeam','Message','MessageSetMessage','MessageSet','Event','EventType','Interaction','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','Account'),pfizerusdev_stage=c('AKT_Message_Topic_Email_Learned','Sent_Email_vod__c_arc'),pfizerprod_cs=c('Approved_Document_vod__c'))
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)

# source script and code setup
# required to run tests
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))

# set required args
productUID <- readModuleConfig(homedir, 'messageSequence','productUID')
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_stage <- getDBConnectionStage(dbuser, dbpassword, dbhost, dbname, port)
con_cs <- getDBConnectionCS(dbuser, dbpassword, dbhost, dbname, port)

# run script
source(sprintf("%s/messageSequence/loadMessageSequenceData.R",homedir))
dta <- loadMessageSequenceData(con,con_l, con_stage,con_cs,productUID)
names(dta) <- paste(names(dta),"_new",sep="")
for(i in 1:length(dta))assign(names(dta)[i],dta[[i]])
# interactions <- dta[[1]]
# accountProduct <- dta[[2]]
# products <- dta[[3]]
# messageSet <- dta[[4]]
# emailTopicNames <- dta[[5]]
# messages <- dta[[6]]
# messageTopic <- dta[[7]]
# interactionsP <- dta[[8]]
# expiredMessages <- dta[[9]]
# accountPredictorNames <- dta[[10]]
# emailTopicNameMap <- dta[[11]]

# disconnect db
dbDisconnect(con)
dbDisconnect(con_cs)
dbDisconnect(con_stage)

# test case
test_that("test have correct length of data", {
  expect_equal(length(dta), 12)
  expect_equal(dim(interactions_new),c(2099,8))
  expect_equal(dim(accountProduct_new),c(309,236))
  expect_equal(dim(products_new),c(1,2))
  expect_equal(dim(messageSetMessage_new),c(10,2))
  expect_equal(dim(messageSet_new),c(4,2))
  expect_equal(length(emailTopicNames_new),29)
  expect_equal(dim(messages_new),c(446,6))
  expect_equal(dim(messageTopic_new),c(85,2))
  expect_equal(dim(interactionsP_new),c(16592,8))
  expect_length(expiredMessages_new,65)
  expect_length(accountPredictorNames_new,39)
  expect_length(emailTopicNameMap_new,18)
})

rm(dta)
test_that("test result is the same as the one saved ", {
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactions.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_accountProduct.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_products.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageSetMessage.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageSet.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_emailTopicNames.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messages.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageTopic.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactionsP.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_expiredMessages.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_accountPredictorNames.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_emailTopicNameMap.RData', homedir))
  expect_equal(interactions_new,interactions)
  expect_equal(accountProduct_new,accountProduct)
  expect_equal(products_new,products)
  expect_equal(messageSetMessage_new,messageSetMessage)
  expect_equal(messageSet_new,messageSet)
  expect_equal(emailTopicNames_new,emailTopicNames)
  expect_equal(messages_new,messages)
  expect_equal(messageTopic_new,messageTopic)
  expect_equal(interactionsP_new,interactionsP)
  expect_equal(expiredMessages_new,expiredMessages)
  expect_equal(accountPredictorNames_new,accountPredictorNames)
  expect_equal(emailTopicNameMap_new,emailTopicNameMap)
})
