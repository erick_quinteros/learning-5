# ###################################################################################
# This is shell script to setup an AWS EC2 instance with all required system level  #
# packages, R, and Python Installation for Aktana learning module.                  #
# You can create an AMI (Amazon Machine Image) from the from the EC2 instance once  #
# the setup is complete and can be used invoke standalone machine or EMR cluster    #
# that can run Aktana learning module.                                              #
#####################################################################################

# Updating system packages
sudo yum update -y

# Installing requried system packages
sudo yum install -y  bzip2-devel.x86_64 xz-devel.x86_64 pcre-devel.x86_64 ibxml2-devel texlive
sudo yum install -y xorg-x11-xauth.x86_64 xorg-x11-server-utils.x86_64 xterm libXt libX11-devel libXt-devel libcurl-devel git compat-gmp4 compat-libffi5
sudo yum install -y gcc gcc-c++ gcc-gfortran
sudo yum install -y readline-devel cairo-devel libpng-devel libjpeg-devel libtiff-devel openssl-devel libxml2-devel mysql-devel mesa-libGLU-devel.x86_64

# Create learning installation directory
mkdir -p /learning
cd /learning

# Building and Installing  R
R_VERSION="R-3.5.2"
R_URL="http://cran.r-project.org/src/base/R-3/$R_VERSION.tar.gz"
pushd .
mkdir R-latest
cd R-latest
wget $R_URL
tar -xzf $R_VERSION.tar.gz
cd $R_VERSION
./configure --with-readline=yes --enable-R-profiling=no --enable-memory-profiling=no --enable-R-shlib --with-pic --prefix=/usr --with-x --with-libpng --with-jpeglib --with-cairo --enable-R-shlib --with-recommended-packages=yes
make -j 8
sudo make install
popd

# NOTE: Epel is Extra Packages for Enterprise Linux (or EPEL) is a Special
# Interest Group that creates, maintains, and manages a high quality set of
# additional packages for Enterprise Linux, including, but not limited to,
# Red Hat Enterprise Linux (RHEL), CentOS and Scientific Linux (SL), Oracle Linux (OL).
# Install Apache-Arrow
sudo yum remove -y epel-release
sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo tee /etc/yum.repos.d/Apache-Arrow.repo <<REPO
[apache-arrow]
name=Apache Arrow
baseurl=https://dl.bintray.com/apache/arrow/centos/7/\$basearch/
gpgcheck=1
enabled=1
gpgkey=https://dl.bintray.com/apache/arrow/centos/RPM-GPG-KEY-apache-arrow
REPO
sudo yum install -y --enablerepo=epel arrow-devel
sudo yum install -y --enablerepo=epel parquet-devel

# Installing MeCab packages
sudo yum install -y https://packages.groonga.org/centos/groonga-release-latest.noarch.rpm
sudo yum makecache
sudo yum install mecab mecab-ipadic mecab-devel
echo "export LD_LIBRARY_PATH='/usr/local/lib'" >> ~/.bashrc

# Installing R Packages
sudo Rscript installPackages.r

# Perform R installation beforehand EMR initialization
# NOTE: If we don't install following packages while using this AMI on EMR. The
#       intialization process will install these packages and make R Default to
#       R version 3.4.2
sudo yum install -y R R-devel R-core R-core-devel R-java R-java-devel

# Rollback to default R 3.5
pushd .
cd /learning/R-latest/$R_VERSION
make install
popd

# Rollback the packages and repositories to defaults
sudo yum remove -y epel-release
sudo yum install -y epel-release
sudo rm -f /etc/yum.repos.d/Apache-Arrow.repo

# Install Python 3.6
sudo yum -y install python36
sudo yum -y install python36-devel

# Install sparkling water
wget http://h2o-release.s3.amazonaws.com/sparkling-water/rel-2.3/27/sparkling-water-2.3.27.zip
unzip sparkling-water-2.3.27.zip

# Installing Python Packages
sudo python3 -m pip install -r requirements.txt
sudo python3 -m pip install h2o_pysparkling_2.3

