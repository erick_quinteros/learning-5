from typing import Any, Union

import mysql.connector
import configparser
import hashlib
import uuid
from pandas.io import sql
from sqlalchemy import create_engine
import pandas as pd
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
import time
import datetime
import logging
import pymysql # NOTE: Required for writing pandas dataframe with SQLAlchemy to MySQL server as MySQL Connector does NOT work.


"""
TO-DO::

1. Exception Handling for database related errors
2. Asserts for the function's input parameters
3. Unit tests for method
4. Update to use Pool Based connections

"""
ACCOUNT_ID_COL = "accountId"
ARCHIVE_TABLE_NAME = "Sent_Email_vod__c_arc"
SIMULATION_INTERACTION_TABLE_NAME = "SimulationAccountSentEmail"
CS_APPROVED_DOCUMENT_TABLE_NAME = "Approved_Document_vod__c"
PHYSICAL_MESSAGE_TABLE_NAME = "AKT_StableMessage_PhysicalMessage"

ALL_PRODUCTS_CONFIG_VALUE = "AKT_ALL_PRODUCTS"

H2O_SERVER_STATUS_AVAILABLE = "AVAILABLE"
H2O_SERVER_STATUS_BUSY = "BUSY"

logger = None


def initialize_logger(simulation_logger_name):

    global logger
    logger = logging.getLogger(simulation_logger_name)

def connect_dse_database():
    """
    This function reads the configuration for the database from file 'dbconfig.ini'.
    Creates a connection to DSE database and returns the connection object for future use.

    :return: Active database Connection object
    """

    # Get database config instance
    db_config = DatabaseConfig.instance()

    # Read the required MySQL configuration
    db_host = db_config.host
    db_user = db_config.user
    db_password = db_config.password
    dse_db_name = db_config.dse_db_name
    db_port = db_config.port

    # Form MySQL Connection
    connection = mysql.connector.connect(host=db_host, user=db_user, password=db_password,
                                         database=dse_db_name, port=db_port)

    return connection


def connect_cs_database():
    """
    This function reads the configuration for the database from file 'dbconfig.ini'.
    Creates a connection to DSE database and returns the connection object for future use.

    :return: Active database Connection object
    """

    # Get database config instance
    db_config = DatabaseConfig.instance()

    # Read the required MySQL configuration
    db_host = db_config.host
    db_user = db_config.user
    db_password = db_config.password
    cs_db_name = db_config.cs_db_name
    db_port = db_config.port

    # Form MySQL Connection
    connection = mysql.connector.connect(host=db_host, user=db_user, password=db_password,
                                         database=cs_db_name, port=db_port)

    return connection


def connect_learning_database():
    """
    This function reads the configuration for the database from file 'dbconfig.ini'.
    Creates a connection to learning database and returns the connection object for future use.

    :return: Active database Connection object
    """

    # Get database config instance
    db_config = DatabaseConfig.instance()

    # Read the required MySQL configuration
    db_host = db_config.host
    db_user = db_config.user
    db_password = db_config.password
    learning_db_name = db_config.learning_db_name
    db_port = db_config.port

    # Form MySQL Connection
    connection = mysql.connector.connect(host=db_host, user=db_user, password=db_password,
                                         database=learning_db_name, port=db_port)

    return connection

def get_learning_db_engine():
    """
    This function returns the sqlalchemy engine for the learning database. It helps to read/write directly from
    pandas data frame to the database.
    :return:
    """

    # Get database config instance
    db_config = DatabaseConfig.instance()

    # Read the required MySQL configuration
    db_host = db_config.host
    db_user = db_config.user
    db_password = db_config.password
    learning_db_name = db_config.learning_db_name
    db_port = db_config.port

    # Generate Connection String
    connection_string = "mysql+pymysql://{user}:{pw}@{host}:{port}/{db}"
    connection_string = connection_string.format(user=db_user, pw=db_password, host=db_host,
                                                 port=db_port, db=learning_db_name)
    # Create engine
    engine = create_engine(connection_string, pool_size=10, max_overflow=20)
    return engine

def get_dse_db_engine():
    """
    This function returns the sqlalchemy engine for the DSE database. It helps to read/write directly from
    pandas data frame to the database.
    :return:
    """

    # Get database config instance
    db_config = DatabaseConfig.instance()

    # Read the required MySQL configuration
    db_host = db_config.host
    db_user = db_config.user
    db_password = db_config.password
    learning_db_name = db_config.dse_db_name
    db_port = db_config.port

    # Generate Connection String
    connection_string = "mysql+pymysql://{user}:{pw}@{host}:{port}/{db}"
    connection_string = connection_string.format(user=db_user, pw=db_password, host=db_host,
                                                 port=db_port, db=learning_db_name)
    # Create engine
    engine = create_engine(connection_string, pool_size=10, max_overflow=20)
    return engine

def get_learning_database_name():
    """
    This function reads the config file and returns the learning database name.
    :return:
    """
    # Intialize the ConfigParser for reading configuration
    config = configparser.ConfigParser()
    config.read('dbconfig.ini')

    learning_database_name = config['MYSQL']['learning_database']

    return learning_database_name


def isTableExists(connection, schema_name, table_name):
    cursor = connection.cursor()
    cursor.execute("""
        SELECT COUNT(*)
        FROM information_schema.tables
        WHERE table_name = '{TABLE_NAME}' AND TABLE_SCHEMA = '{SCHEMA_NAME}';
        """.format(TABLE_NAME=table_name, SCHEMA_NAME=schema_name))

    if cursor.fetchone()[0] == 1:
        cursor.close()
        return True

    cursor.close()
    return False



class SegmentAccessor:

    def get_segment_count(self, build_uid, sim_run_uid):
        """

        :param build_uid:
        :param sim_run_uid:
        :return:
        """

        assert build_uid != "", "Invalid Build UID to process"
        assert sim_run_uid != "", "Invalid Run UID to process"

        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT COUNT(*) FROM SimulationSegment WHERE learningBuildUID='{BUILD_UID}'  AND learningRunUID='{SIM_RUN_UID}';"
        sql_query = sql_query.format(BUILD_UID=build_uid, SIM_RUN_UID=sim_run_uid)

        #print("QUERY: ", sql_query)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        segment_account_count = cursor.fetchone()[0]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return segment_account_count

    def get_segment_account_count(self, build_uid, sim_run_uid):
        """

        :param build_uid:
        :param sim_run_uid:
        :return:
        """

        assert build_uid != "", "Invalid Build UID to process"
        assert sim_run_uid != "", "Invalid Run UID to process"


        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT COUNT(*) FROM SimulationAccountSegment WHERE learningBuildUID='{BUILD_UID}'  AND learningRunUID='{SIM_RUN_UID}';"
        sql_query = sql_query.format(BUILD_UID=build_uid, SIM_RUN_UID=sim_run_uid)

        #print("QUERY: ", sql_query)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        segment_account_count = cursor.fetchone()[0]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return segment_account_count

    def get_account_ids_in_segment(self, feature_name, feature_value):
        """
        This function returns pandas dataframe with account externalIds and name which belongs the the specified segment.
        :param feature_name: Database column name which used for segment
        :param feature_value: Corresponding value for specified database column
        :return: List of account Ids in the segment
        """
        if feature_name == "":
            # Log the error
            logger.error("Invalid feature (column) name for fetch account segment Ids.")

        # Connect to the database
        connection = connect_dse_database()

        # Prepare the query for fetching the account Ids from table
        sql_query = "SELECT externalId, accountName FROM Account WHERE {featureName}='{featureValue}'"
        sql_query = sql_query.format(featureName=feature_name, featureValue=feature_value)
        account_df = pd.read_sql(sql_query, con=connection)

        return account_df

    def get_account_data_table_for_segement(self, feature_name, feature_value):
        """
        This function returns the all the information for the accounts in the segemnt.
        :param feature_name: Database column name which used for segment
        :param feature_value: Corresponding value for specified database column
        :return: List contatning all the information about the account in the database.
        """
        if feature_name == "":
            # Log the error
            logger.error("Invalid feature (column) name for fetch account segment data table.")

        # Connect to the database
        connection = connect_dse_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT * FROM Account where {featureName}='{featureValue}'"
        sql_query = sql_query.format(featureName=feature_name, featureValue=feature_value)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        segment_accounts = cursor.fetchall()

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return segment_accounts

    def get_unique_feature_values(self, feature_name):
        """
        This function will return the list of unique values in the specified column
        :param feature_name:
        :return:
        """

        assert feature_name != "", "Invalid feature_name input for unique feature values"

        # Connect to the database
        connection = connect_dse_database()

        # Prepare the query for fetching distinct values in the column name
        sql_query = "SELECT DISTINCT {featureName} FROM Account"
        sql_query = sql_query.format(featureName=feature_name)

        # Create a cursor and fetch all values
        cursor = connection.cursor()
        cursor.execute(sql_query)
        feature_values = [item[0] for item in cursor.fetchall()]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return feature_values

    def create_account_segments(self, sim_run_uid, mso_build_uid, feature_name):
        """
        This function creates the account segments based specified feature name (i.e. column) in Account table.
        :param mso_build_uid:
        :param feature_name:
        :return:
        """
        assert feature_name != "", "Invalid feature name for creating account segment"

        feature_values = self.get_unique_feature_values(feature_name)

        for feature_value in feature_values:
            self.create_account_segment(sim_run_uid, mso_build_uid, feature_name, feature_value)

        logger.info("Created Segments for feature- " + feature_name)

    def write_segment_account_map(self, sim_run_uid, mso_build_uid, segment_uid, segment_account_ids_names):

        # Get learning database connection
        learning_db_connection = connect_learning_database()

        # Get the time stamp for created and updated timestamps
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')



        sql_query = "INSERT INTO SimulationAccountSegment (learningRunUID, learningBuildUID, segmentUID, accountUID, accountName, " \
                    "createdAt, updatedAt) values ('{SIM_RUN_UID}', '{BUILD_UID}', '{SEGMENT_UID}', %s,  %s, '{CREATED_AT}', '{UPDATED_AT}')"
        sql_query = sql_query.format(SIM_RUN_UID=sim_run_uid, BUILD_UID=mso_build_uid, SEGMENT_UID=segment_uid,
                                     CREATED_AT=timestamp,
                                     UPDATED_AT=timestamp)

        cursor = learning_db_connection.cursor()

        cursor.executemany(sql_query, segment_account_ids_names)

        cursor.close()
        learning_db_connection.commit()
        learning_db_connection.close()

    def create_account_segment(self, sim_run_uid, mso_build_uid, feature_name, feature_value):
        """
        This function will write data in the learning table for segment with feature and it's value
        :param sim_run_uid:
        :param mso_build_uid:
        :param feature_name:
        :param feature_value:
        :return:
        """

        if feature_name == "" or feature_value == "":
            # Log the error
            logger.error("Invalid input for fetch account segment data table.")

        # Get learning database connection
        learning_db_connection = connect_learning_database()

        # Call function to get the data from DSE database
        segment_account_df = self.get_account_ids_in_segment(feature_name, feature_value)

        segment_account_ids_names = list(zip(segment_account_df.externalId, segment_account_df.accountName))

        # Generate random UUID for segment
        segment_uid = uuid.uuid4()

        # Get the time stamp for created and updated timestamps
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        # Create a sql query to insert to the SimulationSegment table
        sql_query = "INSERT INTO SimulationSegment (learningRunUID, learningBuildUID, segmentUID, segmentName, segmentValue, createdAt, updatedAt)" \
                    " VALUES ('{SIM_RUN_UID}', '{BUILD_UID}', '{SEGMENT_UID}', '{SEGMENT_NAME}','{SEGMENT_VALUE}', '{CREATED_AT}', '{UPDATED_AT}')"
        sql_query = sql_query.format(SIM_RUN_UID=sim_run_uid, BUILD_UID=mso_build_uid, SEGMENT_UID=segment_uid, SEGMENT_NAME=feature_name,
                                     SEGMENT_VALUE=feature_value, CREATED_AT=timestamp, UPDATED_AT=timestamp)


        cursor = learning_db_connection.cursor()
        cursor.execute(sql_query)

        sql_query = "INSERT INTO SimulationAccountSegment (learningRunUID, learningBuildUID, segmentUID, accountUID, accountName, " \
                    "createdAt, updatedAt)VALUES ('{SIM_RUN_UID}', '{BUILD_UID}', '{SEGMENT_UID}', %s,  %s, '{CREATED_AT}', '{UPDATED_AT}')"
        sql_query = sql_query.format(SIM_RUN_UID=sim_run_uid, BUILD_UID=mso_build_uid, SEGMENT_UID=segment_uid, CREATED_AT=timestamp,
                                     UPDATED_AT=timestamp)

        cursor.executemany(sql_query, segment_account_ids_names)

        cursor.close()
        learning_db_connection.commit()
        learning_db_connection.close()

        logger.info("Created an Account Segment with Feature Name = '{SEGMENT_NAME}' and Value = '{SEGMENT_VALUE}' "
              "with UID = '{SEGMENT_UID}'".format(SEGMENT_UID=segment_uid, SEGMENT_NAME=feature_name,
                                                  SEGMENT_VALUE=feature_value))

    def get_segment_uids(self, sim_run_uid, build_uid):
        """
        This function will return all unique segment UIDs generated for specified build.
        :param build_uid:
        :return: List of unique segment UIDs for the build
        """
        assert build_uid != "", "Invalid Build UID to process"

        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT DISTINCT SegmentUID FROM SimulationSegment WHERE learningBuildUID='{BUILD_UID}'  AND learningRunUID='{SIM_RUN_UID}';"
        sql_query = sql_query.format(SIM_RUN_UID=sim_run_uid, BUILD_UID=build_uid)

        print("QUERY: ", sql_query)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        segment_uids = [item[0] for item in cursor.fetchall()]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return segment_uids

    def get_account_segment_df(self, mso_build_uid, sim_run_uid):
        """

        :param mso_build_uid:
        :param sim_run_uid:
        :return:
        """

        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT segmentUID, accountUID FROM SimulationAccountSegment WHERE learningBuildUID='{BUILD_UID}' AND learningRunUID='{SIM_RUN_UID}'"
        sql_query = sql_query.format(SIM_RUN_UID=sim_run_uid, BUILD_UID=mso_build_uid)

        segment_account_df = pd.read_sql(sql_query, con=connection)

        return segment_account_df


    def get_accounts_for_segment_uid(self, segment_uid):
        """
        This function will return the list of account UIDs in specified segment.
        :param segment_uid:
        :return: List of account UIDs.
        """
        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT accountUID FROM SimulationAccountSegment WHERE segmentUID='{SEGMENT_UID}';"
        sql_query = sql_query.format(SEGMENT_UID=segment_uid)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        account_uids = [item[0] for item in cursor.fetchall()]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return account_uids

    def write_simulation_account_segment_df(self, segment_account_df):
        """
        This funciton writes the specified data frame to 'SimulationSegmentAccount' table in the learning database.
        It appends to the existing data in the table.
        :param segment_account_df: pandas data frame containing simulation account segment
        :return:
        """

        if segment_account_df.empty:
            logger.info("Empty Simulation Account Segment Dataframe")

        else:
            engine = get_learning_db_engine()

            # Write data frame to the table in db replace will drop the table before inserting
            segment_account_df.to_sql(con=engine, name='SimulationAccountSegment', if_exists='append', index=False, chunksize=10000)

    def write_simulation_segment_df(self, segment_df):
        """
        This funciton writes the specified data frame to 'SimulationSegment' table in the learning database.
        It appends to the existing data in the table.
        :param segment_account_df: pandas data frame containing simulation segment values
        :return:
        """

        if segment_df.empty:
            logger.info("Empty Simulation Account Segment Dataframe")

        else:
            engine = get_learning_db_engine()

            # Write data frame to the table in db replace will drop the table before inserting
            segment_df.to_sql(con=engine, name='SimulationSegment', if_exists='append', index=False, chunksize=500)



    def write_simulation_segment_result(self, sim_run_uid, build_uid, segment_uid, message_seq_uid, score, is_finalized=1):
        """
        This function writes an entry for the segment in SimulationSegmentMessageSequence with specified data.
        :param build_uid:
        :param segment_uid:
        :param message_seq_uid:
        :param is_finalized:
        :return:
        """
        # Connect to the database
        connection = connect_learning_database()

        # Get the time stamp for created and updated timestamps
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        # Prepare the query for fetching the account information from table
        sql_query = "INSERT INTO SimulationSegmentMessageSequence (learningRunUID, learningBuildUID, segmentUID, messageSeqUID, " \
                    "probability, isFinalized, createdAt, updatedAt) VALUES('{SIM_RUN_UID}', '{BUILD_UID}', '{SEGMENT_UID}', " \
                    "'{MSG_SEQ_UID}', '{SCORE}', {IS_FINAL}, '{CREATED_AT}', '{UPDATED_AT}');"
        sql_query = sql_query.format(SIM_RUN_UID=sim_run_uid, BUILD_UID=build_uid, SEGMENT_UID=segment_uid, MSG_SEQ_UID=message_seq_uid,
                                     SCORE=score, IS_FINAL=is_finalized, CREATED_AT=timestamp, UPDATED_AT=timestamp)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)

        # Commit the changes to the database
        connection.commit()

        # Close the cursor and database connection
        cursor.close()
        connection.close()



class DSEAccessor:
    """
    This class is responsible for connecting and retrieving data from DSE database.
    """

    def get_accounts_df(self, column_names):
        """
        This function returns the 'Account' table from DSE database with specified columns in the list.
        :param column_names: list of columns from 'Account' table
        :return:
        """

        if column_names and column_names != "":

            column_names.extend(['externalId', 'accountName'])

            dse_engine = get_dse_db_engine()

            account_df = pd.read_sql_table("Account", dse_engine, columns=column_names)

            return account_df

        else:
            logger.info("Empty column name list to fetch account data frame")

    def get_product_name_map(self):
        """
        This function will read the product table and returns product externalId to name in pandas dataframe.
        :return:
        """
        connection = connect_dse_database()
        cursor = connection.cursor()
        product_name_df = pd.read_sql('SELECT productName, externalId FROM pfizerusprod.Product;', con=connection)
        return product_name_df

    def get_active_undeleted_product_externalIds(self):
        """
        This function returns list of product external ids which are active and not deleted.
        :return: list of product external ids
        """
        sql_query = "SELECT externalId FROM Product WHERE isActive=1 AND isDeleted=0"
        connection = connect_dse_database()
        cursor = connection.cursor()
        cursor.execute(sql_query)

        external_ids = [item[0] for item in cursor.fetchall()]

        cursor.close()
        connection.close()

        return external_ids

    def get_accountId_externalId_map(self):
        """
        This function will return the account Id to external Id in pandas dataframe.
        :return:
        """

        connection = connect_dse_database()
        account_id__df = pd.read_sql('SELECT accountId, externalId FROM Account;', con=connection)
        connection.close()
        return account_id__df

    def get_eventType_df(self):
        """
        This function will return eventType table in pandas dataframe.
        """
        connection = connect_dse_database()
        sql_query = "SELECT * FROM EventType;"
        eventType_df = pd.read_sql(sql_query,con=connection)
        connection.close()
        return eventType_df


    def get_eventCount_df(self, look_back_date_str):
        """
        This function will return event & count since last year
        """
        connection = connect_dse_database()
        sql_query = "select e.`eventTypeId`, v.`eventTypeName`, count(e.`eventTypeId`) as count from Event as e join `EventType` as v on e.`eventTypeId`= v.`eventTypeId` where e.`eventDate`>={DATE} group by e.`eventTypeId`;".format(DATE=look_back_date_str)
        eventCount_df = pd.read_sql(sql_query,con=connection)
        connection.close()
        return eventCount_df

    def get_active_undeleted_message_algorithm_externalIds(self):
        """
        This function returns list of externalIds for active and non-deleted message algorithm ids
        :return:
        """

        sql_query = "SELECT distinct ma.externalId FROM MessageSet ms JOIN (SELECT messageAlgorithmId, externalId FROM " \
                    "MessageAlgorithm where isDeleted=0 and isActive=1 and messageAlgorithmType=1) ma ON ms.messageAlgorithmId=ma.messageAlgorithmId;"
        connection = connect_dse_database()
        cursor = connection.cursor()
        cursor.execute(sql_query)

        external_ids = [item[0] for item in cursor.fetchall()]

        cursor.close()
        connection.close()

        return external_ids

    def get_event_type_id_for_goal(self, goal):
        """

        :param goal:
        :return:
        """
        event = None

        if goal == "OPEN":
            event = "RTE_OPEN"
        elif goal == "CLICK":
            event = "RTE_CLICK"

        sql_query = "SELECT eventTypeId FROM EventType WHERE eventTypeName='{EVENT}'"
        sql_query = sql_query.format(EVENT=event)
        connection = connect_dse_database()
        cursor = connection.cursor()
        cursor.execute(sql_query)

        eventTypeId = cursor.fetchone()[0]

        cursor.close()
        connection.close()

        return eventTypeId

    def get_product_interaction_type_df(self):
        sql_query = "SELECT productInteractionTypeId, productInteractionTypeName FROM ProductInteractionType;"
        connection = connect_dse_database()
        productInteractionType_df = pd.read_sql(sql_query,con=connection)
        connection.close()
        return productInteractionType_df

    def get_call2sampleId_df(self):
        sql_query = "SELECT Id FROM Call2_Sample_vod__c;"
        connection = connect_cs_database()
        call2sampleId_df = pd.read_sql(sql_query,con=connection)
        connection.close()
        return call2sampleId_df


class DatabaseIntializer:

    def initialize_simulation_tables(self):
        """
        This function creates required tables for simulation in learning database specified in the database configuration
        file.
        :return:
        """

        simulatin_sql_script = open("SimulationDatabaseTablesInitialization.sql", 'r')
        init_simulation_sql_commands = simulatin_sql_script.read()
        simulatin_sql_script.close()

        learning_db_name = get_learning_database_name()
        init_simulation_sql_commands = init_simulation_sql_commands.format(databaseName=learning_db_name)

        # Connect to the database
        connection = connect_learning_database()

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(init_simulation_sql_commands, multi=True)

        # Close the cursor and database connection
        cursor.close()
        connection.close()

    def populate_approved_documents_data(self):
        """

        :return:
        """
        logger.info("Preparing approved documents table from archive...")

        # Get database config instance
        db_config = DatabaseConfig.instance()

        # CS db name and connection
        cs_schema_name = db_config.cs_db_name
        cs_db_connection = connect_cs_database()

        # Check if source table exists in the copy storm database
        if not isTableExists(cs_db_connection, cs_schema_name, CS_APPROVED_DOCUMENT_TABLE_NAME):
            logger.info(CS_APPROVED_DOCUMENT_TABLE_NAME + " table not found in copy storm database.")
            cs_db_connection.close()
            return

        # Learning db name and connection
        learning_db_connection = connect_learning_database()
        learning_schema_name = db_config.learning_db_name

        # Check if destination table is created in the learning database
        if not isTableExists(learning_db_connection, learning_schema_name, 'ApprovedDocuments'):
            logger.info("'ApprovedDocuments'table not found in learning database.")
            learning_db_connection.close()
            return

        # Build SQL Query to fetch the source table
        sql_query = "SELECT Id, Product_vod__c, Name, Email_Subject_vod__c, Email_HTML_1_vod__c FROM {TABLE_NAME}"
        sql_query = sql_query.format(TABLE_NAME=CS_APPROVED_DOCUMENT_TABLE_NAME)
        approved_document_df = pd.read_sql(sql_query, con=cs_db_connection)
        cs_db_connection.close()
        logger.info("Fetched data from the copy storm database.")

        # Update the fetched data frame columns
        logger.info("Performing data transformations...")
        approved_document_df.rename(columns={'Id': 'messageUID', 'Product_vod__c': 'productUID', 'Name': 'name',
                                             'Email_Subject_vod__c': 'emailSubject',
                                             'Email_HTML_1_vod__c': 'emailBody'}, inplace=True)

        # Add updated at and created at columns in the data frame
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        approved_document_df['createdAt'] = timestamp
        approved_document_df['updatedAt'] = timestamp

        """
        NOTE: Explicitly truncating the table beforehand and using 'append' method instead of 'replace'. As 'replace'
        will remove all the data and recreate table. It creates table with new email body column as type TEXT instead of
        MEDIUMTEXT which truncates larger text data during insertion.
        """
        # Truncate the `ApprovedDocuments` table in the learning database
        cursor = learning_db_connection.cursor()
        cursor.execute("TRUNCATE TABLE ApprovedDocuments;")
        cursor.close()
        learning_db_connection.close()

        engine = get_learning_db_engine()

        # Write data frame to the table in db replace will drop the table before inserting
        approved_document_df.to_sql(con=engine, name='ApprovedDocuments', if_exists='append', index=False,
                             chunksize=1000)

        logger.info("Completed populating Approved Documents table.")


    def populate_email_data(self, product_uid):
        """

        :return:
        """
        assert product_uid, "Invalid product UID None"

        logger.info("Preparing simulation email table from archive...")

        # Get database config instance
        db_config = DatabaseConfig.instance()
        learning_schema_name = db_config.learning_db_name

        # Create a learning connection
        connection = connect_learning_database()

        if isTableExists(connection, learning_schema_name, ARCHIVE_TABLE_NAME):

            # Build SQL query
            sql_query = "SELECT Id, Account_vod__c, Approved_Email_Template_vod__c,Opened_vod__c, " \
                        "Status_vod__c, Click_Count_vod__c,Email_Config_Values_vod__c, Email_Content_vod__c," \
                        "Email_Sent_Date_vod__c, Sender_Email_vod__c,Account_Email_vod__c, Last_Open_Date_vod__c, " \
                        "Last_Click_Date_vod__c, LastModifiedDate FROM {TABLE_NAME} WHERE Product_vod__c='{PRODUCT_UID}' ;"

            sql_query = sql_query.format(TABLE_NAME=ARCHIVE_TABLE_NAME, PRODUCT_UID=product_uid)

            account_id_df = pd.read_sql(sql_query, con=connection)

            logger.info("Fetched data from the database.")
            logger.info("Performing data transformations...")

            # Get the latest interaction for account id and message id
            account_id_df[account_id_df['LastModifiedDate'].isin(
                account_id_df.groupby(['Account_vod__c', 'Approved_Email_Template_vod__c']).max()['LastModifiedDate'].values)]

            # Eliminate the unnecessary columns
            # account_id_df = account_id_df[['Id', 'Account_vod__c', 'Approved_Email_Template_vod__c', 'Opened_vod__c',
            #                                'Status_vod__c', 'Click_Count_vod__c', 'Email_Config_Values_vod__c', 'Email_Content_vod__c',
            #                                'Email_Sent_Date_vod__c', 'Sender_Email_vod__c', 'Account_Email_vod__c',
            #                                'Last_Open_Date_vod__c', 'Last_Click_Date_vod__c']]

            logger.info("Writing data to database...")

            # Rename the columns to match SimulationAccountSentEmail
            account_id_df.rename(columns={'Id':'InteractionId', 'Account_vod__c':'accountUID', 'Approved_Email_Template_vod__c':'messageUID',
                                          'Opened_vod__c':'isOpen', 'Status_vod__c':'status', 'Click_Count_vod__c':'clickCount',
                                          'Email_Config_Values_vod__c':'emailSubject', 'Email_Content_vod__c':'emailBody',
                                          'Email_Sent_Date_vod__c':'emailSentDate', 'Sender_Email_vod__c':'senderEmailID',
                                          'Account_Email_vod__c':'accountEmailID', 'Last_Open_Date_vod__c':'emailLastOpenedDate',
                                          'Last_Click_Date_vod__c':'lastClickDate'}, inplace=True)


            # Get the time stamp for created and updated timestamps
            ts = time.time()
            timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

            account_id_df['createdAt'] = timestamp
            account_id_df['updatedAt'] = timestamp
            account_id_df = account_id_df.drop('LastModifiedDate', axis=1)

            """
            NOTE: Explicitly truncating the table beforehand and using 'append' method instead of 'replace'. As 'replace'
            will remove all the data and recreate table. It creates table with new email body column as type TEXT instead of
            MEDIUMTEXT which truncates larger text data during insertion.
            """
            cursor = connection.cursor()
            cursor.execute("TRUNCATE TABLE SimulationAccountSentEmail;")
            cursor.close()
            connection.close()

            engine = get_learning_db_engine()

            # Write data frame to the table in db replace will drop the table before inserting
            account_id_df.to_sql(con=engine, name='SimulationAccountSentEmail', if_exists='append',index=False, chunksize=500)

            logger.info("Completed populating simulation email table.")

        else:
            logger.info("Archive Table not found in learning database.")


    def populate_email_data_all_products(self):
        """
        This function loads the email interaction data for all products
        :return:
        """

        # Get database config instance
        db_config = DatabaseConfig.instance()
        learning_schema_name = db_config.learning_db_name

        # Create a learning connection
        connection = connect_learning_database()

        # Check if source table i.e. archive table exists in learning database
        if not isTableExists(connection, learning_schema_name, ARCHIVE_TABLE_NAME):
            print(ARCHIVE_TABLE_NAME + " archive Table not found in learning database.")

        # Check if destination table i.e. interaction table exists in learning database
        if not isTableExists(connection, learning_schema_name, SIMULATION_INTERACTION_TABLE_NAME):
            print(SIMULATION_INTERACTION_TABLE_NAME + " archive Table not found in learning database.")

        # Truncate the target table
        cursor = connection.cursor()
        cursor.execute("TRUNCATE TABLE {TARGET_TABLE};".format(TARGET_TABLE=SIMULATION_INTERACTION_TABLE_NAME))
        print("Truncated the interaction table")

        # Get the time stamp for created and updated timestamps
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        stage_db_name = db_config.dse_db_name + "_stage"

        # Copy the values from source to destination
        sql_query = "INSERT INTO {TARGET_TABLE} (InteractionId, accountUID, messageUID, isOpen, status, " \
                    "clickCount, emailSubject, emailBody, emailSentDate, senderEmailID, accountEmailID, " \
                    "emailLastOpenedDate, lastClickDate, productUID, emailName, createdAt, updatedAt) " \
                    "SELECT Id, Account_vod__c, Approved_Email_Template_vod__c, Opened_vod__c, Status_vod__c, Click_Count_vod__c, " \
                    "Email_Config_Values_vod__c, Email_Content_vod__c, Email_Sent_Date_vod__c, Sender_Email_vod__c, " \
                    "Account_Email_vod__c, Last_Open_Date_vod__c, Last_Click_Date_vod__c, Product_vod__c, p.messageName, " \
                    "'{TIMESTAMP}', '{TIMESTAMP}' FROM  {STAGE_DB}.{SOURCE_TABLE} s LEFT JOIN {STAGE_DB}.{PHYSICAL_MESSAGE_TABLE} p ON p.physicalMessageUID = s.Approved_Email_Template_vod__c;"


        sql_query = sql_query.format(TARGET_TABLE=SIMULATION_INTERACTION_TABLE_NAME, SOURCE_TABLE=ARCHIVE_TABLE_NAME,
                                     STAGE_DB=stage_db_name, PHYSICAL_MESSAGE_TABLE=PHYSICAL_MESSAGE_TABLE_NAME,
                                     TIMESTAMP=timestamp)

        print("Loading data in the simulation interaction table..")
        cursor.execute(sql_query)
        cursor.close()
        connection.commit()
        connection.close()

        print("Populating interaction table is complete!")

    def populate_approved_documents_data_global(self):
        """
        This function loads the data into `ApprovedDocuments` table in the learning database.
        :return:
        """

        # Get database config instance
        db_config = DatabaseConfig.instance()

        # CS db name and connection
        cs_schema_name = db_config.cs_db_name
        cs_db_connection = connect_cs_database()

        # Check if source table exists in the copy storm database
        if not isTableExists(cs_db_connection, cs_schema_name, CS_APPROVED_DOCUMENT_TABLE_NAME):
            cs_db_connection.close()
            return

        # Learning db name and connection
        learning_db_connection = connect_learning_database()
        learning_schema_name = db_config.learning_db_name

        # Check if destination table is created in the learning database
        if not isTableExists(learning_db_connection, learning_schema_name, 'ApprovedDocuments'):
            learning_db_connection.close()
            return

        # Build SQL Query to fetch the source table
        sql_query = "SELECT Id, Product_vod__c, Name, Email_Subject_vod__c, Email_HTML_1_vod__c FROM {TABLE_NAME}"
        sql_query = sql_query.format(TABLE_NAME=CS_APPROVED_DOCUMENT_TABLE_NAME)
        approved_document_df = pd.read_sql(sql_query, con=cs_db_connection)
        cs_db_connection.close()

        # Update the fetched data frame columns
        approved_document_df.rename(columns={'Id': 'messageUID', 'Product_vod__c': 'productUID', 'Name': 'name',
                                             'Email_Subject_vod__c': 'emailSubject',
                                             'Email_HTML_1_vod__c': 'emailBody'}, inplace=True)

        # Add updated at and created at columns in the data frame
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        approved_document_df['createdAt'] = timestamp
        approved_document_df['updatedAt'] = timestamp

        """
        NOTE: Explicitly truncating the table beforehand and using 'append' method instead of 'replace'. As 'replace'
        will remove all the data and recreate table. It creates table with new email body column as type TEXT instead of
        MEDIUMTEXT which truncates larger text data during insertion.
        """
        # Truncate the `ApprovedDocuments` table in the learning database
        cursor = learning_db_connection.cursor()
        cursor.execute("TRUNCATE TABLE ApprovedDocuments;")
        cursor.close()
        learning_db_connection.close()

        engine = get_learning_db_engine()

        # Write data frame to the table in db replace will drop the table before inserting
        approved_document_df.to_sql(con=engine, name='ApprovedDocuments', if_exists='append', index=False,
                             chunksize=1000)



class MessageAccessor:
    """
    This class is responsible for message and message sequences related calls to the database
    """

    def get_message_seq_count(self, build_uid, sim_run_uid):
        """

        :param build_uid:
        :param sim_run_uid:
        :return:
        """

        assert build_uid != "", "Invalid Build UID to process"
        assert sim_run_uid != "", "Invalid Run UID to process"

        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT COUNT(*) FROM SimulationMessageSequence WHERE learningBuildUID='{BUILD_UID}'  AND learningRunUID='{SIM_RUN_UID}';"
        sql_query = sql_query.format(BUILD_UID=build_uid, SIM_RUN_UID=sim_run_uid)

        # print("QUERY: ", sql_query)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        message_seq_count = cursor.fetchone()[0]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return message_seq_count

    def get_account_message_seq_count(self, build_uid, sim_run_uid):
        """

        :param build_uid:
        :param sim_run_uid:
        :return:
        """

        assert build_uid != "", "Invalid Build UID to process"
        assert sim_run_uid != "", "Invalid Run UID to process"

        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT COUNT(*) FROM SimulationAccountMessageSequence WHERE learningBuildUID='{BUILD_UID}'  AND learningRunUID='{SIM_RUN_UID}';"
        sql_query = sql_query.format(BUILD_UID=build_uid, SIM_RUN_UID=sim_run_uid)

        # print("QUERY: ", sql_query)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        account_message_seq_count = cursor.fetchone()[0]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return account_message_seq_count

    def get_segment_message_seq_count(self, build_uid, sim_run_uid):
        """

        :param build_uid:
        :param sim_run_uid:
        :return:
        """

        assert build_uid != "", "Invalid Build UID to process"
        assert sim_run_uid != "", "Invalid Run UID to process"

        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT COUNT(*) FROM SimulationSegmentMessageSequence WHERE learningBuildUID='{BUILD_UID}'  AND learningRunUID='{SIM_RUN_UID}';"
        sql_query = sql_query.format(BUILD_UID=build_uid, SIM_RUN_UID=sim_run_uid)

        # print("QUERY: ", sql_query)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        segment_message_seq_count = cursor.fetchone()[0]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return segment_message_seq_count

    def get_message_ids_for_message_set(self, message_set_id):
        """
        This will return list of message Ids in the specified message set.
        :param message_set_id:
        :return:
        """
        # Connect to the database
        connection = connect_dse_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT lastPhysicalMessageUID FROM Message WHERE messageChannelId = 1 AND messageId IN (SELECT messageId FROM MessageSetMessage where MessageSetId='{MESSAGESETID}')"
        sql_query = sql_query.format(MESSAGESETID=message_set_id)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        message_last_physical_uids = [item[0] for item in cursor.fetchall()]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return message_last_physical_uids

    # TO-DO:: Move to DSEAccessor Class
    def get_product_id_for_uid(self, product_uid):
        """
        This function returns product id for specified product UID (i.e. externalId) from product table.
        :param product_uid:
        :return:
        """
        assert product_uid != "", "Invalid Product UID to fetch Message sets"

        # Connect to the database
        connection = connect_dse_database()

        # Prepare the query to fetch the product id
        sql_query = "SELECT productId FROM Product WHERE externalId='{PRODUCT_UID}';"
        sql_query = sql_query.format(PRODUCT_UID=product_uid)

        # Create a cursor and execute query
        cursor = connection.cursor()
        cursor.execute(sql_query)

        product_id = cursor.fetchone()[0]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return product_id

    def get_active_message_algorithms_for_product(self, product_uid):
        """
        This function returns all active message algorithms for the specified product.
        :param product_uid:
        :return:
        """
        assert product_uid != "", "Invalid Product UID to fetch Message sets"

        # Get product id from product UID
        product_id = self.get_product_id_for_uid(product_uid)

        # Connect to the database
        connection = connect_dse_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT messageAlgorithmId FROM MessageAlgorithm WHERE productId={PRODUCT_ID} AND isActive=1 AND isDeleted=0;"
        sql_query = sql_query.format(PRODUCT_ID=product_id)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        active_message_algorithms = [item[0] for item in cursor.fetchall()]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return active_message_algorithms

    def get_non_expired_message_list(self, product_uid):
        """

        :param product_uid:
        :return:
        """
        assert product_uid != "", "Invalid Product UID to fetch Messages"

        # Get product id from product UID
        product_id = self.get_product_id_for_uid(product_uid)
        db_config = DatabaseConfig.instance()

        sql_query = "SELECT lastPhysicalMessageUID from {DSE_DB_NAME}.Message WHERE messageChannelId =1 AND messageId IN" \
                    " (SELECT messageId FROM {DSE_DB_NAME}.MessageSetMessage where MessageSetId IN " \
                    "(SELECT messageSetId FROM {DSE_DB_NAME}.MessageSet WHERE productId={PRODUCT_ID})) AND lastPhysicalMessageUID IN " \
                    "(SELECT Id FROM {CS_DB_NAME}.Approved_Document_vod__c WHERE Product_vod__c='{PRODUCT_UID}' " \
                    "AND Status_vod__c='Approved_vod' AND Email_Subject_vod__c IS NOT NULL) "

        sql_query = sql_query.format(DSE_DB_NAME=db_config.dse_db_name, CS_DB_NAME=db_config.cs_db_name,
                                     PRODUCT_ID=product_id, PRODUCT_UID=product_uid)

        # Connect to the database
        connection = connect_dse_database()

        # Create a cursor and fetch all messages
        cursor = connection.cursor()
        cursor.execute(sql_query)
        last_physical_message_uids = [item[0] for item in cursor.fetchall()]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return last_physical_message_uids

    def get_active_message_sets_for_product(self, product_uid):
        """
        This function returns the list active message sets for specified product UID
        :param product_uid:
        :return:
        """
        assert product_uid != "", "Invalid Product UID to fetch Message sets"

        # Get Active message algorithms
        message_algorithm_ids = self.get_active_message_algorithms_for_product(product_uid)

        # Prepare message Algorithm Ids string (Remove square brackets to make correct sql query)
        message_algorithm_ids_str = str(message_algorithm_ids)[1:-1]

        # Connect to the database
        connection = connect_dse_database()

        # Prepare the query for fetching the account information from table
        sql_query = "SELECT messageSetId FROM MessageSet WHERE messageAlgorithmId IN ({ALGORITHM_IDS});"

        sql_query = sql_query.format(ALGORITHM_IDS=message_algorithm_ids_str)

        #logger.debug(sql_query)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        message_set_ids = [item[0] for item in cursor.fetchall()]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return message_set_ids

    def get_ordered_messages_for_sequence(self, message_sequence_id):
        """
        This function will return ordered list of message Ids in the specified message sequence. It will return list in
        order [First Message Id, Second Message Id, ...]
        :param message_sequence_id:
        :return:
        """

        # Prepare SQL Query
        sql_query = "SELECT messageId, sequenceOrder FROM SimulationMessageSequence WHERE messageSeqUID='{message_sequence_id}';"
        sql_query = sql_query.format(message_sequence_id = message_sequence_id)

        # connect to database
        connection = connect_learning_database()
        cursor = connection.cursor()

        cursor.execute(sql_query)
        message_list = cursor.fetchall()

        # Close the connections
        cursor.close()
        connection.close()

        return message_list

    def write_message_sequences_v2(self, sim_run_uid, mso_build_uid, unique_message_sequence_list):
        """
        This function writes the provided list of message sequences to the database and returns corresponding map of
        sequence string to generated sequence uuid.
        :param sim_run_uid:
        :param mso_build_uid:
        :param unique_message_sequence_list:
        :return: dictionary of message sequence string to generated uuid
        """

        # Get the time stamp for created and updated timestamps
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        # SQL query template for batch insert
        sql_query_template = "INSERT INTO SimulationMessageSequence (learningBuildUID, learningRunUID, messageSeqUID, messageSeqHash, messageID, " \
                             "sequenceOrder, createdAt, updatedAt) VALUES('{LEARNING_BUILD_UID}', '{SIMULATION_RUN_UID}', " \
                             "%s, %s, %s, %s, '{CREATED_AT}', '{UPDATED_AT}')"
        sql_query_template = sql_query_template.format(LEARNING_BUILD_UID=mso_build_uid, SIMULATION_RUN_UID=sim_run_uid,
                                                       CREATED_AT=timestamp, UPDATED_AT=timestamp)
        input_data = []

        message_sequence_str_to_id_map = {}

        for message_sequence_str in unique_message_sequence_list:
            # Omitting first element in the list as message sequence string has ';' at the start
            message_sequence = message_sequence_str.split(';')[1:]

            message_sequence_id = uuid.uuid4()  # TO-DO:: Unique ID generation
            message_sequence_hash = self.get_hash_for_message_sequence(message_sequence)

            message_sequence_str_to_id_map[message_sequence_str] = str(message_sequence_id)

            for message_order, message_id in enumerate(message_sequence):
                input_data.append((str(message_sequence_id), message_sequence_hash, message_id, message_order + 1))

        # connect to database
        connection = connect_learning_database()
        cursor = connection.cursor()

        # Execute batch command
        cursor.executemany(sql_query_template, input_data)

        # Commit the changes to the database
        connection.commit()

        # Close the connections
        cursor.close()
        connection.close()

        return message_sequence_str_to_id_map

    def write_message_sequences(self, sim_run_uid, mso_build_uid, message_sequence_list):
        """
        This function will execute SQL Batch INSERT command to write all the message sequences in the specified list of
        message sequences.
        :param message_sequence_list:
        :return:
        """
        # TO-DO:: Update to reuse existing sequences

        # Get the time stamp for created and updated timestamps
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        # SQL query template for batch insert
        sql_query_template = "INSERT INTO SimulationMessageSequence (learningBuildUID, learningRunUID, messageSeqUID, messageSeqHash, messageID, " \
                             "sequenceOrder, createdAt, updatedAt) VALUES('{LEARNING_BUILD_UID}', '{SIMULATION_RUN_UID}', " \
                             "%s, %s, %s, %s, '{CREATED_AT}', '{UPDATED_AT}')"
        sql_query_template = sql_query_template.format(LEARNING_BUILD_UID=mso_build_uid, SIMULATION_RUN_UID=sim_run_uid,
                                                       CREATED_AT=timestamp, UPDATED_AT=timestamp)
        input_data = []

        # Fill the input data for insert
        for sequence_index, message_sequence in enumerate(message_sequence_list):
            message_sequence_id = uuid.uuid4() # TO-DO:: Unique ID generation
            message_sequence_hash = self.get_hash_for_message_sequence(message_sequence)

            # TO-DO:: Check if message sequence already exists in the table
            for message_order, message_id in enumerate(message_sequence):
                input_data.append((str(message_sequence_id), message_sequence_hash, message_id, message_order+1))

        # connect to database
        connection = connect_learning_database()
        cursor = connection.cursor()

        # Execute batch command
        cursor.executemany(sql_query_template, input_data)

        # Commit the changes to the database
        connection.commit()

        # Close the connections
        cursor.close()
        connection.close()

    def get_hash_for_message_sequence(self, message_sequence_list):
        """
        This function will return a hash code for provided message sequence
        :param message_sequence_list:
        :return:
        """
        # Create a message sequence string by concatenating the sequences
        message_sequence_string = "".join(message_sequence_list)

        # Calculate the Hash using MD5 in Hashlib
        md5 = hashlib.md5()
        md5.update(message_sequence_string.encode("UTF-8"))

        return md5.hexdigest()

    def get_unique_message_sequence_uids(self, sim_run_uid, mso_build_uid):
        """
        This function will return unique message sequences present in the SimulationMessageSequence table
        :return:
        """
        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching
        sql_query = "SELECT DISTINCT messageSeqUID FROM SimulationMessageSequence WHERE learningBuildUID='{BUILD_UID}' " \
                    "AND learningRunUID='{SIM_RUN_UID}';"

        sql_query = sql_query.format(SIM_RUN_UID=sim_run_uid, BUILD_UID=mso_build_uid)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        unique_message_seq_uids = [item[0] for item in cursor.fetchall()]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return unique_message_seq_uids

    def write_account_simulation_results(self, sim_run_uid, build_uid, result_df):
        """
        This function writes the simulation results for accounts in the database.
        :param build_uid:
        :param prediction_result_df:
        :return:
        """
        assert build_uid != "", "Invalid Build UID to write"

        # Get the time stamp for created and updated timestamps
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        prediction_result_df = pd.DataFrame(result_df)

        prediction_result_df.fillna(0, inplace=True)
        prediction_result_df = prediction_result_df.melt(id_vars=[ACCOUNT_ID_COL])
        prediction_result_df.rename(index=str, columns={"variable": "messageSeqUID", "value": "probability", "accountId": "accountUID"}, inplace=True)
        prediction_result_df['learningRunUID'] = str(sim_run_uid)
        prediction_result_df['learningBuildUID'] = str(build_uid)
        prediction_result_df['isFinalized'] = 0
        prediction_result_df['createdAt'] = timestamp
        prediction_result_df['updatedAt'] = timestamp

        # Set finalized true for best sequences
        finalized_indices = prediction_result_df.groupby(['accountUID'])['probability'].idxmax()
        prediction_result_df.at[finalized_indices, 'isFinalized'] = 1
        prediction_result_df = prediction_result_df[prediction_result_df.isFinalized == 1]

        engine = get_learning_db_engine()

        prediction_result_df.to_sql(con=engine, name='SimulationAccountMessageSequence', if_exists='append', index=False)

    def write_account_simulation_results_v2(self, sim_run_uid, build_uid, result_df):
        """
        This function writes the simulation results for accounts in the database.
        :param build_uid:
        :param prediction_result_df:
        :return:
        """
        assert build_uid != "", "Invalid Build UID to write"

        # Get the time stamp for created and updated timestamps
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        result_df.fillna(0, inplace=True)
        result_df.loc[:, 'learningRunUID'] = str(sim_run_uid)
        result_df.loc[:, 'learningBuildUID'] = str(build_uid)
        result_df.loc[:, 'isFinalized'] = 1
        result_df.loc[:, 'createdAt'] = timestamp
        result_df.loc[:, 'updatedAt'] = timestamp

        acct_uids = result_df['accountUID']

        # create a tmp table of accountUID and join Account to get accountName quickly
        conn_dse = connect_dse_database()

        cursor_dse = conn_dse.cursor()
        cursor_dse.execute('DROP TABLE IF EXISTS tmp_account')
        cursor_dse.execute('CREATE TABLE tmp_account (rowId int AUTO_INCREMENT, accountUID varchar(80), PRIMARY KEY (rowId))')
        cursor_dse.executemany('INSERT INTO tmp_account (accountUID) VALUES(%s)', list(zip(acct_uids)))

        cursor_dse.execute('SELECT accountName FROM Account a, tmp_account t WHERE a.externalId = t.accountUID ORDER BY rowId')
        account_names = cursor_dse.fetchall()
        result_df['accountName'] = list(account_names)

        cursor_dse.execute('DROP TABLE IF EXISTS tmp_account')

        cursor_dse.close()
        conn_dse.close()

        engine = get_learning_db_engine()

        result_df.to_sql(con=engine, name='SimulationAccountMessageSequence', if_exists='append', index=False)


    def get_finalized_sequence_and_score_for_accounts(self, sim_run_uid, mso_build_uid):
        """
        This method will returns pandas datafram ewiht
        :param sim_run_uid:
        :param build_uid:
        :return:
        """
        connection = connect_learning_database()
        cursor = connection.cursor()
        sql_query = "SELECT accountUID, messageSeqUID, probability FROM SimulationAccountMessageSequence WHERE " \
                    "isFinalized = 1 AND learningRunUID = '{SIM_RUN_UID}' AND learningBuildUID = '{BUILD_UID}';"
        sql_query = sql_query.format(SIM_RUN_UID=sim_run_uid, BUILD_UID=mso_build_uid)

        sim_account_messsage_seq_df = pd.read_sql(sql_query, con=connection)

        return sim_account_messsage_seq_df


    def get_finalized_sequence_and_score_for_account(self, account_uid, sim_run_uid, build_uid):
        """
        This function will return finalized message sequence and corresponding score for specified account and the build.
        :param account_uid:
        :param build_uid:
        :return: Finalized message sequence UID and Probability score the sequence
        """

        """
        # FOR UNIT TEST ONLY USING account_id instead of account_uid
        connection = connect_dse_database()

        sql_query = "SELECT accountId FROM Account WHERE externalId='{ACCOUNT_UID}';"
        sql_query = sql_query.format(ACCOUNT_UID=account_uid)

        print(sql_query)

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        result = cursor.fetchone()
        if result:
            account_uid = result[0]

        cursor.close()
        connection.close()
        """

        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching
        sql_query = "SELECT messageSeqUID FROM SimulationAccountMessageSequence WHERE accountUId='{ACCOUNT_UID}' AND learningBuildUID='{BUILD_UID}' AND learningRunUID='{SIM_RUN_UID}' AND isFinalized=1;"
        sql_query = sql_query.format(ACCOUNT_UID=account_uid, BUILD_UID=build_uid, SIM_RUN_UID=sim_run_uid)

        #print(sql_query)

        finalized_message_seq_uid = None

        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        result = cursor.fetchone()
        if result:
            finalized_message_seq_uid = result[0]

        cursor.close()

        # Prepare the query for fetching
        sql_query = "SELECT probability FROM SimulationAccountMessageSequence WHERE accountUId='{ACCOUNT_UID}' AND learningBuildUID='{BUILD_UID}' AND isFinalized=1;"
        sql_query = sql_query.format(ACCOUNT_UID=account_uid, BUILD_UID=build_uid)

        probability = 0
        # Create a cursor and fetch all account information
        cursor = connection.cursor()
        cursor.execute(sql_query)
        result = cursor.fetchone()
        if result:
            probability = result[0]

        # Close the cursor and database connection
        cursor.close()
        connection.close()

        return finalized_message_seq_uid, probability


class LearningAccessor:
    """


    """

    def clean_up_simulation(self, mso_build_uid, current_sim_run_uid):
        """
        This function removes the data for all the previous runs for the specified build
        :param mso_build_uid:
        :param current_sim_run_uid:
        :return:
        """

        connection = connect_learning_database()
        cursor = connection.cursor(buffered=True)

        sql_query_template = "DELETE FROM {TABLE_NAME} WHERE learningBuildUID = '{MSO_BUILD_UID}' AND learningRunUID != '{SIM_RUN_UID}'"

        logger.debug("Removing rows from SimulationAccountMessageSequence")
        sql_query = sql_query_template.format(TABLE_NAME="SimulationAccountMessageSequence", MSO_BUILD_UID=mso_build_uid, SIM_RUN_UID=current_sim_run_uid)
        cursor.execute(sql_query)

        logger.debug("Removing rows from SimulationSegmentMessageSequence")
        sql_query = sql_query_template.format(TABLE_NAME="SimulationSegmentMessageSequence", MSO_BUILD_UID=mso_build_uid, SIM_RUN_UID=current_sim_run_uid)
        cursor.execute(sql_query)

        logger.debug("Removing rows from SimulationMessageSequence")
        sql_query = sql_query_template.format(TABLE_NAME="SimulationMessageSequence", MSO_BUILD_UID=mso_build_uid, SIM_RUN_UID=current_sim_run_uid)
        cursor.execute(sql_query)

        logger.debug("Removing rows from SimulationAccountSegment")
        sql_query = sql_query_template.format(TABLE_NAME="SimulationAccountSegment", MSO_BUILD_UID=mso_build_uid, SIM_RUN_UID=current_sim_run_uid)
        cursor.execute(sql_query)

        logger.debug("Removing rows from SimulationSegment")
        sql_query = sql_query_template.format(TABLE_NAME="SimulationSegment", MSO_BUILD_UID=mso_build_uid, SIM_RUN_UID=current_sim_run_uid)
        cursor.execute(sql_query)

        connection.commit()
        cursor.close()
        connection.close()

    def clear_mso_optimal_predictor_tables(self):
        """

        :return:
        """
        connection = connect_learning_database()
        cursor = connection.cursor()

        sql_query_template = "TRUNCATE TABLE {TABLE_NAME};"

        sql_query = sql_query_template.format(TABLE_NAME="MessageSequencePredictorInfo")
        cursor.execute(sql_query)

        sql_query_delete_template  = "DELETE FROM {TABLE_NAME} WHERE `paramName` LIKE 'LE_MS%';"
        sql_query_delete = sql_query_delete_template.format(TABLE_NAME="OptimalLearningParams")
        cursor.execute(sql_query_delete)

        connection.commit()
        cursor.close()
        connection.close()

    def clear_tte_optimal_predictor_tables(self):
        """

        :return:
        """
        connection = connect_learning_database()
        cursor = connection.cursor()

        sql_query_template = "TRUNCATE TABLE {TABLE_NAME};"

        sql_query = sql_query_template.format(TABLE_NAME="MessageTimingPredictorInfo")
        cursor.execute(sql_query)

        sql_query_delete_template  = "DELETE FROM {TABLE_NAME} WHERE `paramName` LIKE 'LE_MT%';"
        sql_query_delete = sql_query_delete_template.format(TABLE_NAME="OptimalLearningParams")
        cursor.execute(sql_query_delete)

        connection.commit()
        cursor.close()
        connection.close()

    def get_enabled_products_for_learning(self):
        """
        This function queries learning schema and find out all the enabled products for the customer. If the enabled
        product list contains AKT_ALL_PRODUCTS then we fetch externalIds for all the active and un-deleted products from
        the `Product` table in DSE schema.
        :return: List of externalIds for the enabled product.
        """
        sql_query = "SELECT configurationValue FROM CustomerConfigurations WHERE configurationType='productsEnabled';"

        connection = connect_learning_database()
        cursor = connection.cursor()
        cursor.execute(sql_query)
        enabled_products = [item[0] for item in cursor.fetchall()]

        if ALL_PRODUCTS_CONFIG_VALUE in enabled_products:
            dse_accessor = DSEAccessor()
            enabled_products = dse_accessor.get_active_undeleted_product_externalIds()

            if ALL_PRODUCTS_CONFIG_VALUE in enabled_products: enabled_products.remove(ALL_PRODUCTS_CONFIG_VALUE)

        return enabled_products

    def get_optimal_learning_parameters(self, product_uid, channel_uid, goal):
        """
        This function fetches and returns optimal parameters for learning for specified product, channel and goal.

        :param product_uid:
        :param channel_uid:
        :param goal:
        :return: It will return list of tuple where [(paramName1, paramValue1), (paramName2, paramValue2), ..].
        """
        sql_query = "SELECT paramName, paramValue FROM OptimalLearningParams WHERE productUID='{PRODUCT_UID}' " \
                    "AND channelUID='{CHANNEL_UID}' AND goal='{GOAL}';"
        sql_query = sql_query.format(PRODUCT_UID=product_uid, CHANNEL_UID=channel_uid, GOAL=goal)

        connection = connect_learning_database()
        cursor = connection.cursor()
        cursor.execute(sql_query)
        optimal_parameters = cursor.fetchall()

        return optimal_parameters

    def get_available_h2o_server(self):
        """
        This function reads `h2odockerinfo` table in learning database and returns ip and port for the available server.
        :return: ip and port as strings for available server if no server is available returns None
        """

        ip = None
        port = None

        connection = connect_learning_database()

        sql_query = "SELECT ip, port, id FROM h2oDockerInfo WHERE status='{SERVER_STATUS}';"
        sql_query = sql_query.format(SERVER_STATUS=H2O_SERVER_STATUS_AVAILABLE)

        cursor = connection.cursor(buffered=True) # Buffered true required to avoid 'Unread result found.' error on cursor.
        cursor.execute(sql_query)
        result = cursor.fetchone()

        if result:
            ip = result[0]
            port = result[1]
            server_id = result[2]

            # Close the previous cursor with result and create a new one
            cursor.close()
            cursor = connection.cursor()

            # Mark the H2O server busy
            sql_query = "UPDATE h2oDockerInfo SET status='{SERVER_BUSY_STATUS}' WHERE id={ID}"
            sql_query = sql_query.format(SERVER_BUSY_STATUS=H2O_SERVER_STATUS_BUSY, ID=server_id)
            cursor.execute(sql_query)

        connection.commit()
        cursor.close()
        connection.close()

        return ip, port

    def release_h2o_server(self, ip, port):
        """
        This function releases the specified H2O server with ip and port. It mark it as available in the h2oDockerInfo table
        :param ip: ip address of the server
        :param port: port number for H2O
        :return:
        """

        assert ip != None , "Invalid IP Address for H2O server"
        assert ip != "", "Invalid IP Address for H2O server"
        assert port != None, "Invalid Port number for H2O server"

        connection = connect_learning_database()

        sql_query = "UPDATE h2oDockerInfo SET status='{SERVER_AVAILABLE_STATUS}' WHERE ip='{IP}' AND port={PORT}"
        sql_query = sql_query.format(SERVER_AVAILABLE_STATUS=H2O_SERVER_STATUS_AVAILABLE, IP=ip, PORT=port)

        cursor = connection.cursor()
        cursor.execute(sql_query)

        connection.commit()
        cursor.close()
        connection.close()

    def get_account_message_interactions(self, productUID):
        """
        This function returns account message interactions from `SimulationAccountSentEmail` for specified productUID.
        :param productUID: Product UID to filter the data
        :return: Pandas data frame containing columns 'accountUID', 'messageUID', 'status' and 'emailSentDate' columns
        """

        # Connect to the database
        connection = connect_learning_database()

        # Prepare the query for fetching the account Ids from table
        sql_query = "SELECT accountUID, messageUID, status, emailSentDate FROM SimulationAccountSentEmail WHERE productUID='{PRODUCT_UID}';"
        sql_query = sql_query.format(PRODUCT_UID=productUID)
        account_message_interaction_df = pd.read_sql(sql_query, con=connection)

        connection.close()

        return account_message_interaction_df



    def write_simulation_run(self, simulation_run_uid, mso_build_uid, version_uid, config_uid,
                             run_type="SIMULATION", execution_status="running"):
        """

        :param simulation_run_uid:
        :param mso_build_uid:
        :param version_uid:
        :param config_uid:
        :param run_type:
        :param execution_status:
        :return:
        """
        # Get the time stamp for created and updated timestamps
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        # Prepare SQL Query
        sql_query = "INSERT INTO LearningRun (learningBuildUID, learningRunUID, learningVersionUID, learningConfigUID, " \
                    "runType, executionStatus, executionDateTime, createdAt, updatedAt) VALUES ('{LEARNING_BUILD_UID}', " \
                    "'{SIMULATION_RUN_UID}', '{VERSION_UID}', '{CONFIG_UID}', '{RUN_TYPE}', '{EXEC_STATUS}', " \
                    "'{EXEC_TIMESTAMP}', '{CREATED_AT}', '{UPDATED_AT}')"

        sql_query = sql_query.format(LEARNING_BUILD_UID=mso_build_uid, SIMULATION_RUN_UID=simulation_run_uid,
                                     VERSION_UID=version_uid, CONFIG_UID=config_uid, RUN_TYPE=run_type, EXEC_STATUS=execution_status,
                                     EXEC_TIMESTAMP=timestamp, CREATED_AT=timestamp, UPDATED_AT=timestamp)

        # connect to database
        connection = connect_learning_database()
        cursor = connection.cursor()

        cursor.execute(sql_query)

        connection.commit()

        # Close the connections
        cursor.close()
        connection.close()

    def update_simulation_execution_status(self, simulation_run_uid, status):
        """

        :param simulation_run_uid:
        :param status:
        :return:
        """
        # Get the time stamp for created and updated timestamps
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        sql_query = "UPDATE LearningRun SET executionStatus = '{EXEC_STATUS}', updatedAt='{UPDATED_AT}' WHERE " \
                    "learningRunUID = '{SIM_RUN_UID}';"

        sql_query = sql_query.format(EXEC_STATUS=status, UPDATED_AT=timestamp, SIM_RUN_UID=simulation_run_uid)

        # connect to database
        connection = connect_learning_database()
        cursor = connection.cursor()

        cursor.execute(sql_query)

        connection.commit()

        # Close the connections
        cursor.close()
        connection.close()


    def get_last_updated_for_sent_email_archive_table(self):
        """
        This function will return the last updated date for the 'SimulationAccountSentEmail' table from learning database.
        :return:
        """
        # connect to database
        connection = connect_learning_database()
        cursor = connection.cursor()

        """
        NOTE: All the rows in the 'SimulationAccountSentEmail' table are populated from archive table at same time. Hence
        all the entries will have same 'updatedAt' timestamp.
        """
        sql_query = "SELECT updatedAt FROM SimulationAccountSentEmail LIMIT 1;"

        cursor.execute(sql_query)
        time_stamp = cursor.fetchone()

        # Check if query returned no result
        if time_stamp:
            time_stamp = time_stamp[0]

        logger.debug("Last updated timestamp for sent email archive table = " + str(time_stamp))

        cursor.close()
        connection.close()

        return time_stamp

    def get_last_updated_for_approved_documents_table(self):
        """
        This function will return the last updated date for the 'SimulationAccountSentEmail' table from learning database.
        :return:
        """
        # connect to database
        connection = connect_learning_database()
        cursor = connection.cursor()

        """
        NOTE: All the rows in the 'SimulationAccountSentEmail' table are populated from archive table at same time. Hence
        all the entries will have same 'updatedAt' timestamp.
        """
        sql_query = "SELECT updatedAt FROM ApprovedDocuments LIMIT 1;"

        cursor.execute(sql_query)
        time_stamp = cursor.fetchone()

        # Check if query returned no result
        if time_stamp:
            time_stamp = time_stamp[0]

        logger.debug("Last updated timestamp for sent email archive table = " + str(time_stamp))

        cursor.close()
        connection.close()

        return time_stamp


    def write_eventType_df(self, eventType_df):

        db_config = DatabaseConfig.instance()

        # CS db name and connection
        learning_schema_name = db_config.learning_db_name
        learning_db_connection = connect_learning_database()

        # Check if table exists. If so, delete it
        if isTableExists(learning_db_connection, learning_schema_name, "EventTypeSelect"):
            cursor = learning_db_connection.cursor()
            sql_query = "TRUNCATE TABLE EventTypeSelect;"
            cursor.execute(sql_query)
            cursor.close()
            logger.info("EventTypeSelect Table Truncated")

        engine = get_learning_db_engine()

        # Write data frame to the table in db replace will drop the table before inserting
        eventType_df.to_sql(con=engine, name='EventTypeSelect', if_exists='append', index=False,
                             chunksize=1000)

        logger.info("EventTypeSelect Table updated")
