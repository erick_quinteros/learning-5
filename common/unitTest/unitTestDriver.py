#Called by bin/runUnitTest.sh for python testing purposes
#Runs all tests for a module or just a single test file


import unittest
import sys
import os
import subprocess


if __name__ == '__main__':
    for arg in sys.argv:
        arg = arg.split("=")
        if len(arg) == 2:
            exec(arg[0] + " = " + arg[1])
    abstestdir = homedir + "/" + testdir
    sys.path.append(abstestdir)
    import tests
    for item in os.listdir(abstestdir):
        if item.endswith(".py") and item.startswith("test_"):
            if func == "all":
                unittest.main(module=item[:-3], argv=[ testdir + "/" + item ], exit=False)
            elif ("test_" + func + ".py") == item:
                unittest.main(module=item[:-3], argv=[ testdir + "/" + item ])
