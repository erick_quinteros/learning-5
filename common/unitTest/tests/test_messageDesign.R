context('testing messageDesign from learningPackage')
print(Sys.time())

# loading Learning package to call function for test
library(Learning)

# load required data
# load(sprintf('%s/common/unitTest/data/for_messageDesign.RData',homedir))
load(sprintf('%s/messageSequence/tests/data/from_messageSequence_prepareDynamicDesignMatrix_ints.RData', homedir))

# test cases
targetName <- "OPEN___a3RA0000000e486MAA"
sendName <- "SEND___a3RA0000000e486MAA"
result <- messageDesign(ints,sendName,targetName)
test_that("check the dimension of result for message a3RA0000000e486MAA", {
  expect_equal(dim(result),c(72,8)) # (75,8) before remove eventTimeUTC and take unique from events by time instead of day
  expect_length(unique(result$accountId),68)
  expect_length(eval(parse(text=sprintf("unique(result[result$%s>0,]$account)",sendName))),68) # 65 before new sort in messageDesign (account: 226531  63337 383508  54538 437737 252733 missed)
  expect_equal(eval(parse(text=sprintf("dim(result[result$%s>0,])",sendName))),c(71,8))
  expect_equal(colSums(result[,-1]), c(OPEN___a3RA0000000e0wBMAQ=1, OPEN___a3RA0000000e486MAA=21, priorVisit=724, SEND___a3RA0000000e0u5MAA=1, SEND___a3RA0000000e0wBMAQ=2, SEND___a3RA0000000e47gMAA=1, SEND___a3RA0000000e486MAA=71))
})

targetName <- "OPEN___a3RA00000001MtAMAU"
sendName <- "SEND___a3RA00000001MtAMAU"
result <- messageDesign(ints,sendName,targetName)
test_that("check the dimension of result for message a3RA00000001MtAMAU", {
  expect_equal(dim(result),c(227,4)) # (236,4) before remove eventTimeUTC and take unique from events by time instead of day
  expect_length(unique(result$accountId),222)
  expect_length(eval(parse(text=sprintf("unique(result[result$%s>0,]$account)",sendName))),222) # 216 before new sort in messageDesign (account: 40496  56174 343998 missed)
  expect_equal(eval(parse(text=sprintf("dim(result[result$%s>0,])",sendName))),c(227,4))
  expect_equal(colSums(result[,-1]), c(OPEN___a3RA00000001MtAMAU=20, priorVisit=3370, SEND___a3RA00000001MtAMAU=227))
})