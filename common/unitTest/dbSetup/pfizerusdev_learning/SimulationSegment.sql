CREATE TABLE `SimulationSegment` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `segmentUID` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `segmentName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `segmentValue` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`segmentUID`),
  KEY `SimulationSegment_fk_1` (`learningBuildUID`),
  KEY `SimulationSegment_fk_2` (`learningRunUID`)
  /* CONSTRAINT `SimulationSegment_fk_1` FOREIGN KEY (`learningBuildUID`) REFERENCES `LearningBuild` (`learningBuildUID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SimulationSegment_fk_2` FOREIGN KEY (`learningRunUID`) REFERENCES `LearningRun` (`learningRunUID`) ON DELETE CASCADE ON UPDATE CASCADE */
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
