CREATE TABLE `MessageRescoringTimes` (
  `accountId` int(11) DEFAULT NULL,
  `physicalMessageUID` char(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `messageId` int(11) DEFAULT NULL,
  `learningBuildUID` char(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rescoringTimes` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci